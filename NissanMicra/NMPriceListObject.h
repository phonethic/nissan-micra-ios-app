//
//  NMPriceListObject.h
//  NissanMicra
//
//  Created by Rishi on 21/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMPriceListObject : NSObject
@property (nonatomic, copy) NSString *priceId;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *PET_XL;
@property (nonatomic, copy) NSString *PET_XL_O;
@property (nonatomic, copy) NSString *PET_XV_CVT;
@property (nonatomic, copy) NSString *DL_XE;
@property (nonatomic, copy) NSString *DL_XL;
@property (nonatomic, copy) NSString *DL_XL_O;
@property (nonatomic, copy) NSString *DL_XV;
@property (nonatomic, copy) NSString *DL_XV_P;
@property (nonatomic, copy) NSString *ACT_XE;
@property (nonatomic, copy) NSString *ACT_XL;
@property (nonatomic, copy) NSString *ACT_XV;
@property (nonatomic, copy) NSString *ACT_XV_S;
@end
