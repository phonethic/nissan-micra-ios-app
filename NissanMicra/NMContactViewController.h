//
//  NMContactViewController.h
//  NissanMicra
//
//  Created by Rishi on 20/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface NMContactViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    BOOL oneTimeAnimation;
}

@property (strong, nonatomic) IBOutlet UITableView *contactTableView;
@property (strong, nonatomic) NSArray *contactList;
@end
