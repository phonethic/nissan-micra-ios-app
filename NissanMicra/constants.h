
#ifndef _ALERT_MESSAGES_H_
#define _ALERT_MESSAGES_H_
#import "Flurry.h"
#import "Reachability.h"

#define ALERT_TITLE @"Nissan Micra"
//#define HELVETICA_FONT(SIZE) [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:SIZE]
//#define HELVETICA_FONT_SMALL(SIZE) [UIFont fontWithName:@"HelveticaNeue-Light" size:SIZE]

//#define HELVETICA_FONT(SIZE) [UIFont  boldSystemFontOfSize:SIZE]
//#define HELVETICA_FONT_SMALL(SIZE) [UIFont systemFontOfSize:SIZE]

#define HELVETICA_FONT(SIZE) [UIFont fontWithName:@"NissanAG" size:SIZE]

//Red #define DEFAULT_COLOR [UIColor colorWithRed:163/255.0 green:0/255.0 blue:32/255.0 alpha:1]
#define DEFAULT_RED 30/255.0
#define DEFAULT_GREEN 93/255.0
#define DEFAULT_BLUE 125/255.0
#define DEFAULT_COLOR [UIColor colorWithRed:DEFAULT_RED green:DEFAULT_GREEN blue:DEFAULT_BLUE alpha:1]

#define BACKGROUD_COLOR  [UIColor whiteColor]

#define TABLE_SELECTION_COLOR [UIColor colorWithRed:53/255.0 green:53/255.0 blue:52/255.0 alpha:1]

#define PETROL @"Petrol"
#define DIESEL @"Diesel"
#define ACTIVE @"Active"

#define TABLE_SEPERATOR_COLOR [UIColor lightGrayColor]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define DEFAULTS_GALLERY @"Gallery"
#define DEFAULTS_GALLERYTOHOME @"GalleryToHome"

#define NETWORK_NOTIFICATION @"NetWorkNotification"
#endif
