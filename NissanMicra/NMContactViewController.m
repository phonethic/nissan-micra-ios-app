//
//  NMContactViewController.m
//  NissanMicra
//
//  Created by Rishi on 20/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMContactViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "NMAppDelegate.h"
#import "NMWebViewViewController.h"
#import "CarUtilStoreListViewController.h"


#define TESTDRIVE_TEL_TEXT @"Call to book a Test Drive"
#define TESTDRIVE_EMAIL_TEXT @"Mail to book a Test Drive"
#define CUSTSUPPORT_TEL_TEXT @"Call Nissan Customer Support"
#define CUSTSUPPORT_EMAIL_TEXT @"Email Customer Support"
#define BREAKDOWN_TEL_TEXT @"Call breakdown Helpline"
#define BREAKDOWN_EMAIL_TEXT @"Email breakdown location"
#define FACEBOOK_TEXT @"Nissan India on facebook"
#define DEALER_TEXT @"Dealer Locator"


#define TESTDRIVE_TEL @"tel://18002094080"
#define CUSTSUPPORT_TEL @"tel://18002094080"
#define CUSTSUPPORT_EMAILID @"customercare.nissan@hai.net.in"
#define BREAKDOWN_TEL @"tel://18002094080"

@interface NMContactViewController ()

@end

@implementation NMContactViewController
@synthesize contactTableView;
@synthesize contactList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Contact_Tab_Event"];

        [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    self.contactTableView.backgroundColor = BACKGROUD_COLOR;

    
    contactList = [[NSArray alloc] initWithObjects:FACEBOOK_TEXT,DEALER_TEXT, nil];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    DebugLog(@"viewDidAppear");
    if (!oneTimeAnimation) {
        [self.contactTableView reloadData];
        [self reloadData:TRUE];
        oneTimeAnimation = TRUE;
    }
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contactList count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier =  @"contactCell";//[NSString stringWithFormat:@"contact%d%d",indexPath.section,indexPath.row];
    UIImageView *cellImageView;
    UILabel *textLabel;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        DebugLog(@"cell == nil %@",CellIdentifier);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.tag = indexPath.row;
        cell.hidden = TRUE;
        
        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10 , cell.frame.origin.y +10, 30, 30)];
        cellImageView.contentMode = UIViewContentModeScaleToFill;
        cellImageView.backgroundColor =  [UIColor clearColor];
        cellImageView.tag = 11;
        [cell.contentView addSubview:cellImageView];
        
        textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+5, cell.frame.origin.y+10, 270, 30)];
        textLabel.tag = 21;
        textLabel.font = HELVETICA_FONT(18.0);
        textLabel.textColor = [UIColor darkGrayColor];
        textLabel .lineBreakMode =  UILineBreakModeTailTruncation;
        textLabel.textAlignment = UITextAlignmentLeft;
        textLabel.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:textLabel];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_SELECTION_COLOR;
        cell.selectedBackgroundView = bgColorView;
    }
    
    //    cell.textLabel.text = [contactList objectAtIndex:indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleBlue;
    
    NSString *text = [contactList objectAtIndex:indexPath.row];
    
    DebugLog(@"text%@",text);
    cellImageView = (UIImageView *)[cell viewWithTag:11];
    if ([text isEqualToString:TESTDRIVE_TEL_TEXT] || [text isEqualToString:CUSTSUPPORT_TEL_TEXT] || [text isEqualToString:BREAKDOWN_TEL_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_tel.png"];
    }
    else if ([text isEqualToString:CUSTSUPPORT_EMAIL_TEXT] || [text isEqualToString:TESTDRIVE_EMAIL_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_mail.png"];
    }
    else if ([text isEqualToString:BREAKDOWN_EMAIL_TEXT]|| [text isEqualToString:DEALER_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_location.png"];
    }
    else if ([text isEqualToString:FACEBOOK_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_fb.png"];
    }
    
    textLabel = (UILabel *)[cell viewWithTag:21];
    textLabel.text = text;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DebugLog(@"row=%d",indexPath.row);

    
    NSString *selectedText =  [contactList objectAtIndex:indexPath.row];
    if ([selectedText isEqualToString:TESTDRIVE_TEL_TEXT])
    {
        [self bookTestDriveBtnPressed];
    }
    else if ([selectedText isEqualToString:FACEBOOK_TEXT])
    {
        if([NM_APP_DELEGATE networkavailable])
        {
            [self openWebSitebtnPressed];
        }
        else
        {
            UIAlertView *errorView = [[UIAlertView alloc]
                                       initWithTitle: ALERT_TITLE
                                       message:@"Please check your internet connection and try again."
                                       delegate:self
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
            [errorView show];
        }
    }
    else if ([selectedText isEqualToString:TESTDRIVE_EMAIL_TEXT])
    {
        [self emailToBookTestDrive];
    }
    else if ([selectedText isEqualToString:DEALER_TEXT])
    {
        [self storeLocatebtnPressed];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark internal methods
- (void)bookTestDriveBtnPressed
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Do you want to call Test Drive center number ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:1];
        [ alert show ];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
    }
}

#pragma Mail Composer Methods
- (void)emailToBookTestDrive
{
    @try{
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        picker.navigationBar.tintColor = DEFAULT_COLOR;
        [picker setToRecipients:[NSArray arrayWithObject:CUSTSUPPORT_EMAILID]];
        [picker setSubject:@"Test Drive"];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        //DebugLog(@"Exception %@",exception.reason);
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result == MFMailComposeResultSent)
    {
        [Flurry logEvent:@"TestDrive_Email_Event"];
    }
    [controller dismissModalViewControllerAnimated:YES];
}

- (void)openWebSitebtnPressed
{
    NMWebViewViewController *webSiteViewController = [[NMWebViewViewController alloc] initWithNibName:@"NMWebViewViewController" bundle:nil] ;
    webSiteViewController.title = @"Facebook";
    [self.navigationController pushViewController:webSiteViewController animated:YES];
}
- (void)storeLocatebtnPressed
{
    CarUtilStoreListViewController *storeLocListViewController = [[CarUtilStoreListViewController alloc] initWithNibName:@"CarUtilStoreListViewController" bundle:nil] ;
    storeLocListViewController.title = @"Dealers";
    [self.navigationController pushViewController:storeLocListViewController animated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Contact Number Dialed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif
    if (buttonIndex == 1)
    {
        NSString *phoneNumber = nil;
        switch (alertView.tag) {
            case 1:
            {
                phoneNumber = TESTDRIVE_TEL;
                [Flurry logEvent:@"TestDrive_Call_Event"];
            }
                break;
           default:
                break;
        }
        UIApplication *myApp = [UIApplication sharedApplication];
        [myApp openURL:[NSURL URLWithString:phoneNumber]];
        return;
    }
}



- (void)reloadData:(BOOL)animated
{
    
    DebugLog(@"reloadData With Animation");
    
    NSArray *indexPaths = [self.contactTableView indexPathsForVisibleRows];
    DebugLog(@"indexArray %@",indexPaths);
    [self reloadCellWithAnimation:0 indexPathsArray:indexPaths];
}

//-(void)reloadCellWithAnimation:(int)index
-(void)reloadCellWithAnimation:(int)index indexPathsArray:(NSArray *)indexArray
{
   
    if (index == indexArray.count) {
        return;
    }
    UITableViewCell *cell = (UITableViewCell *)[contactTableView cellForRowAtIndexPath:[indexArray objectAtIndex:index]];
    DebugLog(@"reloadCellWithAnimation cell.tag %d: cell %@ ===> cell.hidden %d",cell.tag,[indexArray objectAtIndex:index],cell.hidden);
    
    if (cell.tag < contactList.count)
    {
        cell.hidden = TRUE;
        [cell setFrame:CGRectMake(320, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
        
        // [UIView transitionWithView:cell duration:0.2 options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
        [UIView animateWithDuration:0.2 animations:^(void) {
            cell.tag = cell.tag + contactList.count;
            cell.hidden = FALSE;
            [cell setFrame:CGRectMake(-20, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
        }
                         completion:^(BOOL finished){
                             [UIView animateWithDuration:0.15 animations:^{
                                 [cell setFrame:CGRectMake(0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
                             }];
                             // [self reloadCellWithAnimation:index+1];
                             [self reloadCellWithAnimation:index+1 indexPathsArray:indexArray];
                         }];
    }
    else
    {
        [self reloadCellWithAnimation:index+1 indexPathsArray:indexArray];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    DebugLog(@"scrollViewDidEndDecelerating");
    [self reloadData:YES];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    DebugLog(@"scrollViewDidEndDragging");
    [self reloadData:YES];
}


#pragma Device Orientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}
-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setContactTableView:nil];
    [super viewDidUnload];
}
@end
