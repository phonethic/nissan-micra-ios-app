//
//  CarUtilDealerStore.m
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CarUtilDealerStore.h"

@implementation CarUtilDealerStore
@synthesize storeId,storeName,storeAddress,storeEmail,storeWebsite,storePhone1,storePhone2,storeFax,storeMobile1,storeMobile2,storePhotos,storeDescription,storeLatitude,storeLongitute;

-(id)init
{
    self = [super init];
    
    if(self) {
        self.storePhotos = [[NSMutableArray alloc] init];
    }
	return self;
}
@end
