//
//  NMSplashViewController.h
//  NissanMicra
//
//  Created by Rishi on 24/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSlidingPagesDataSource.h"
//#define SPLASH_LINK @"http://192.168.254.203:81/splash.xml"
#define SPLASH_LINK @"http://nissanmicra.co.in/neon-micra/splash.xml"

@interface NMSplashViewController : UIViewController <TTSlidingPagesDataSource,NSXMLParserDelegate> {
    NSURLConnection *conn;
    NSMutableData *responseAsyncData;
    NSXMLParser *xmlParser;
    TTScrollSlidingPagesController *slider;
    
    //Glow Animation
    NSTimer *animationTimer;
    int animationTimerCount;
    CGFloat gradientLocations[3];
    
    BOOL TAP_DETECTED;
    int currentPageIndex;
    BOOL stopAnimation;
}
@property (strong, nonatomic) IBOutlet UIButton *goBtn;
@property (strong, nonatomic) NSMutableArray *imagelinkArray;
@property (strong, nonatomic) IBOutlet UIImageView *logoimageView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView1;
@property (strong, nonatomic) IBOutlet UIImageView *imageView2;
@property (nonatomic, strong) NSTimer *autoAnimationTimer;

- (IBAction)goBtnPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *glowBtn;
- (IBAction)glowBtnClicked:(id)sender;
@end
