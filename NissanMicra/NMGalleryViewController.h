//
//  NMGalleryViewController.h
//  NissanMicra
//
//  Created by Kirti Nikam on 10/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"

@class CarUtilGallery;
@interface NMGalleryViewController : UIViewController<NSXMLParserDelegate,MWPhotoBrowserDelegate>
{
    NSURLConnection *conn;
    NSMutableData *responseAsyncData;
    NSXMLParser *xmlParser;
    int status;
    CarUtilGallery *galleryObj;
    BOOL elementFound;
}
@property (retain, nonatomic) NSMutableArray *galleryArray;
@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, copy) NSString *socialLink;

@end
