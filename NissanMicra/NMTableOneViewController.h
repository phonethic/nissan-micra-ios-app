//
//  NMTableOneViewController.h
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface NMTableOneViewController : UIViewController
{
    sqlite3 *micraDB;
}
@property (nonatomic,readwrite) int showdiffSelected;
@property (nonatomic,readwrite) int hideSimSelected;
@property (copy,nonatomic) NSString *selectedHeader;
@property (copy,nonatomic) NSString *category;
@property (strong, nonatomic) NSMutableArray *objectArray;
@property (strong, nonatomic) IBOutlet UITableView *tableOneView;
@end
