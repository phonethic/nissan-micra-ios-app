//
//  NMWebViewViewController.h
//  NissanMicra
//
//  Created by Rishi on 20/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMWebViewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barbackBtn;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barfwdBtn;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barrefreshBtn;
@property (retain, nonatomic) IBOutlet UIToolbar *webviewtoolBar;

- (IBAction)webviewbackBtnPressed:(id)sender;
- (IBAction)webviewfwdBtnPressed:(id)sender;
- (IBAction)webviewrefreshBtnPressed:(id)sender;
@end
