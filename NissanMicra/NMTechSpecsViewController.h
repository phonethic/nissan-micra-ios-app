//
//  NMTechSpecsViewController.h
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface NMTechSpecsViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate>
{
     sqlite3 *micraDB;
     UIActionSheet *actionSheet;
}

@property (strong, nonatomic) IBOutlet UITableView *techspecsTableView;
@property (strong, nonatomic) UIPickerView *modelPickerView;
@property (copy,nonatomic) NSString *category;
@property (strong, nonatomic) NSMutableArray *objectArray;
@property (strong, nonatomic) NSMutableArray *modelObjectsArray;
@property (strong, nonatomic) IBOutlet UIButton *showdiffBtn;
@property (strong, nonatomic) IBOutlet UIButton *hidesimBtn;

- (IBAction)showdiffBtnPressed:(id)sender;
- (IBAction)hidesimBtnPressed:(id)sender;
@end
