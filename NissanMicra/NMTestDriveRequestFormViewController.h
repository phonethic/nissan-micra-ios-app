//
//  NMTestDriveRequestFormViewController.h
//  NissanMicra
//
//  Created by Kirti Nikam on 04/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMTestDriveRequestFormViewController : UIViewController <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSMutableData *responseAsyncData;
    UIActionSheet *actionSheet;
    
    NSURLConnection *connection;
}
@property (strong, nonatomic) NSMutableArray *modelArray;
@property (copy,nonatomic) NSString *selectedCategory;
@property (copy,nonatomic) NSString *responseString;

@property (strong, nonatomic) UIPickerView *modelPickerView;
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *mobileTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *modelTextField;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *trasparentView;
@property (strong, nonatomic) IBOutlet UIImageView *downarrowImageView;
- (IBAction)submitBtnClicked:(id)sender;
- (IBAction)EditingChanged:(id)sender;
@end
