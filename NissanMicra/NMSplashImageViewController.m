//
//  NMSplashImageViewController.m
//  NissanMicra
//
//  Created by Rishi on 24/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMSplashImageViewController.h"
#import "UIImageView+WebCache.h"

@interface NMSplashImageViewController ()

@end

@implementation NMSplashImageViewController
@synthesize imglink;
@synthesize splashimageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [splashimageView setImageWithURL:[NSURL URLWithString:self.imglink]
                   placeholderImage:[UIImage imageNamed:@"splash.jpg"]
                            success:^(UIImage *image) {
                                //DebugLog(@"success");
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSplashimageView:nil];
    [super viewDidUnload];
}
@end
