//
//  NMPriceListObject.m
//  NissanMicra
//
//  Created by Rishi on 21/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMPriceListObject.h"

@implementation NMPriceListObject
@synthesize priceId;
@synthesize state;
@synthesize city;
@synthesize PET_XL;
@synthesize PET_XL_O;
@synthesize PET_XV_CVT;
@synthesize DL_XE;
@synthesize DL_XL;
@synthesize DL_XL_O;
@synthesize DL_XV;
@synthesize DL_XV_P;
@synthesize ACT_XE;
@synthesize ACT_XL;
@synthesize ACT_XV;
@synthesize ACT_XV_S;
@end
