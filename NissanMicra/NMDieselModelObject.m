//
//  NMDieselModelObject.m
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMDieselModelObject.h"

@implementation NMDieselModelObject
@synthesize Feature;
@synthesize XE;
@synthesize XL;
@synthesize XL_O;
@synthesize XV;
@synthesize XV_Pre;
@synthesize is_same;
@end
