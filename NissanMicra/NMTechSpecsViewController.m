//
//  NMTechSpecsViewController.m
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "NMAppDelegate.h"
#import "NMTechSpecsViewController.h"
#import "MFSideMenu.h"
#import "NMActiveModelObject.h"
#import "NMDieselModelObject.h"
#import "NMPetrolModelObject.h"

@interface NMTechSpecsViewController ()

@end

@implementation NMTechSpecsViewController
@synthesize objectArray;
@synthesize techspecsTableView;
@synthesize category;
@synthesize modelPickerView;
@synthesize modelObjectsArray;
@synthesize showdiffBtn,hidesimBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.techspecsTableView.backgroundColor = BACKGROUD_COLOR;

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithTitle:@"MODEL" style:UIBarButtonSystemItemDone target:self action:@selector(model_Clicked:)];
    techspecsTableView.autoresizesSubviews = TRUE;
    [self changeButtonFontAndTextColor:showdiffBtn];
    [self changeButtonFontAndTextColor:hidesimBtn];
    [showdiffBtn setTitle:@"Show Differences" forState:UIControlStateNormal];
    [hidesimBtn setTitle:@"Hide Similarities" forState:UIControlStateNormal];

    modelObjectsArray = [[NSMutableArray alloc] init];
    [modelObjectsArray addObject:PETROL];
    [modelObjectsArray addObject:DIESEL];
    [modelObjectsArray addObject:ACTIVE];
    
    category = [modelObjectsArray objectAtIndex:0];
    self.navigationItem.leftBarButtonItem.title = category;
    [self loadObjectArray];
    
    [self addPickerWithDoneButton]; //Adding picker for choosing Car models
    
    [self setupMenuBarButtonItems];
    
    DebugLog(@"category -%@-",category);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:category,@"Model",nil];
    [Flurry logEvent:@"Technical_Specifications_Tab_Event" withParameters:params];
    
}

-(void)changeButtonFontAndTextColor:(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:TABLE_SELECTION_COLOR forState:UIControlStateHighlighted];
    [btn setBackgroundColor:[UIColor grayColor]];
    btn.titleLabel.font = HELVETICA_FONT(14);
    btn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
    btn.layer.borderWidth = 2.0;
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Bar Button method
- (void) model_Clicked:(id)sender {
    UISegmentedControl *doneButton = (UISegmentedControl *)[actionSheet viewWithTag:1000];
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    modelPickerView.frame = CGRectMake(modelPickerView.frame.origin.x,modelPickerView.frame.origin.y,self.view.frame.size.width,216);
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, self.view.frame.size.width,485)];
}

#pragma mark Picker Button method
- (void)doneFilterBtnClicked:(id)sender
{
    category = [modelObjectsArray objectAtIndex:[modelPickerView selectedRowInComponent:0]];
    self.navigationItem.leftBarButtonItem.title = category;
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    [self loadObjectArray];
    
    DebugLog(@"category -%@-",category);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:category,@"Model",nil];
    [Flurry logEvent:@"Technical_Specifications_Tab_Event" withParameters:params];
}

- (void)cancelFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma Model Picker method
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    modelPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    modelPickerView.showsSelectionIndicator = YES;
    modelPickerView.dataSource = self;
    modelPickerView.delegate = self;
    [actionSheet addSubview:modelPickerView];
    
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    doneButton.tag = 1000;
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    
    [modelPickerView reloadAllComponents];
    if([modelPickerView numberOfRowsInComponent:0] != -1 && modelObjectsArray.count > 0)
    {
        [modelPickerView selectRow:0 inComponent:0 animated:YES];
        category = [modelObjectsArray objectAtIndex:0];
    }
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [modelObjectsArray count];
}

//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
//{
//    return self.view.frame.size.width;
//}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    //DebugLog(@"titleForRow: categorypickerarray %@",categoryObjectsArray);
    return [modelObjectsArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
}

#pragma mark Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    DebugLog(@"W==%f H==%f",self.view.frame.size.width,self.view.frame.size.height);
    int width = 0;
    if([category isEqualToString:ACTIVE])
    {
        width = 15;
    }else if([category isEqualToString:DIESEL])
    {
        width = 25;
    }
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,45)];
    customView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel0 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel0.backgroundColor = [UIColor clearColor];
    headerLabel0.frame = CGRectMake(0, 0, 140, 45);
    headerLabel0.font = HELVETICA_FONT(12);
    headerLabel0.text = @"Features";
    headerLabel0.textAlignment = UITextAlignmentCenter;
    headerLabel0.layer.borderWidth = 1.0;
    headerLabel0.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel0.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel0.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel0];
    
    UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel1.backgroundColor = [UIColor clearColor];
    headerLabel1.frame = CGRectMake(CGRectGetMaxX(headerLabel0.frame), 0, 60-width, 45);
    headerLabel1.font = HELVETICA_FONT(12);
    if([category isEqualToString:PETROL])
        headerLabel1.text = @"XL";
    else
        headerLabel1.text = @"XE";
    headerLabel1.textAlignment = UITextAlignmentCenter;
    headerLabel1.layer.borderWidth = 1.0;
    headerLabel1.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel1.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel1.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel1];
    
    UILabel *headerLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel2.backgroundColor = [UIColor clearColor];
    headerLabel2.frame = CGRectMake(CGRectGetMaxX(headerLabel1.frame), 0, 60-width, 45);
    headerLabel2.font = HELVETICA_FONT(12);
    if([category isEqualToString:PETROL])
        headerLabel2.text = @"XL(0)";
    else
        headerLabel2.text = @"XL";
    headerLabel2.textAlignment = UITextAlignmentCenter;
    headerLabel2.layer.borderWidth = 1.0;
    headerLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel2.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel2.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel2];
    
    UILabel *headerLabel3 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel3.backgroundColor = [UIColor clearColor];
    headerLabel3.frame = CGRectMake(CGRectGetMaxX(headerLabel2.frame), 0, 60-width, 45);
    headerLabel3.font = HELVETICA_FONT(12);
    if([category isEqualToString:PETROL])
        headerLabel3.text = @"XV CVT";
    else if([category isEqualToString:DIESEL])
        headerLabel3.text = @"XL(0)";
    else
        headerLabel3.text = @"XV";
    headerLabel3.textAlignment = UITextAlignmentCenter;
    headerLabel3.layer.borderWidth = 1.0;
    headerLabel3.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel3.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel3.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel3];
    
    if([category isEqualToString:ACTIVE] || [category isEqualToString:DIESEL])
    {
        UILabel *headerLabel4 = [[UILabel alloc] initWithFrame:CGRectZero];
        headerLabel4.backgroundColor = [UIColor clearColor];
        headerLabel4.frame = CGRectMake(CGRectGetMaxX(headerLabel3.frame), 0, 60-width, 45);
        headerLabel4.font = HELVETICA_FONT(12);
        if([category isEqualToString:DIESEL])
            headerLabel4.text = @"XV";
        else
            headerLabel4.text = @"XV(S)";
        headerLabel4.textAlignment = UITextAlignmentCenter;
        headerLabel4.layer.borderWidth = 1.0;
        headerLabel4.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        headerLabel4.layer.borderColor = [UIColor blackColor].CGColor;
        headerLabel4.textColor = [UIColor blackColor];
        [customView addSubview:headerLabel4];
    
        if([category isEqualToString:DIESEL])
        {
            UILabel *headerLabel5 = [[UILabel alloc] initWithFrame:CGRectZero];
            headerLabel5.backgroundColor = [UIColor clearColor];
            headerLabel5.frame = CGRectMake(CGRectGetMaxX(headerLabel4.frame), 0, 60-width+5, 45);
            headerLabel5.font = HELVETICA_FONT(12);
            if([category isEqualToString:DIESEL])
                headerLabel5.text = @"XV(P)";
            
            headerLabel5.textAlignment = UITextAlignmentCenter;
            headerLabel5.layer.borderWidth = 1.0;
            headerLabel5.layer.borderColor = [UIColor blackColor].CGColor;
            headerLabel5.textColor = [UIColor blackColor];
            headerLabel5.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            [customView addSubview:headerLabel5];
        }
    }
    return customView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//     if(![category isEqualToString:ACTIVE] && !hidesimBtn.isSelected) {
//        return [objectArray count] + 1;
//    }else{
//         return [objectArray count];
//     }
    if(!hidesimBtn.isSelected) {
        return [objectArray count] + 1;
    }else{
        return [objectArray count];
    }
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    DebugLog(@"W==%f H==%f",self.view.frame.size.width,self.view.frame.size.height);
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //DebugLog(@"W==%f H==%f",self.view.frame.size.width,self.view.frame.size.height);
    DebugLog(@"index path row = %d",indexPath.row);
   // if (![category isEqualToString:ACTIVE] && !hidesimBtn.isSelected && indexPath.row == [objectArray count]) {
    if (!hidesimBtn.isSelected && indexPath.row == [objectArray count]) {
        static NSString *CellIdentifier = @"tableFootercell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *cellLabel1;
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            
            cellLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
            cellLabel1.backgroundColor = [UIColor clearColor];
            cellLabel1.frame = CGRectMake(0,0,320,45);
            cellLabel1.font = HELVETICA_FONT(12);
            cellLabel1.tag = 1;
            cellLabel1.numberOfLines = 2;
            cellLabel1.textColor = [UIColor blackColor];
            cellLabel1.textAlignment = UITextAlignmentCenter;
            cellLabel1.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            [cell.contentView addSubview:cellLabel1];
        }
        cellLabel1 = (UILabel *)[cell viewWithTag:1];
        
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cellLabel1.text = @"* Test result of rule 115 CMVR 1989";
        
        return cell;
    }
    
    BOOL same = NO;
    if([category isEqualToString:ACTIVE])
    {
        NMActiveModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
        same = tempObt.is_same;
    }else if([category isEqualToString:PETROL]){
        NMPetrolModelObject *tempObt = (NMPetrolModelObject *)[objectArray objectAtIndex:indexPath.row];
        same = tempObt.is_same;
    }
    else {
        NMDieselModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
        same = tempObt.is_same;
    }
    
    if(same)
    {
        UILabel *cellLabel1;
        UILabel *cellLabel2;

        static NSString *CellIdentifier = @"tablesamecell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            DebugLog(@"id = %@",CellIdentifier);
            
            cellLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
            cellLabel1.backgroundColor = [UIColor clearColor];
            cellLabel1.frame = CGRectMake(2, 0, 140, 45);
            cellLabel1.font = HELVETICA_FONT(12);
            cellLabel1.tag = 1;
            cellLabel1.numberOfLines = 2;
            cellLabel1.textColor = [UIColor blackColor];
            cellLabel1.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            [cell.contentView addSubview:cellLabel1];
            
            cellLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
            cellLabel2.backgroundColor = [UIColor clearColor];
            cellLabel2.frame = CGRectMake(CGRectGetMaxX(cellLabel1.frame),0,180,45);
            cellLabel2.font = HELVETICA_FONT(12);
            cellLabel2.tag = 2;
            cellLabel2.numberOfLines = 4;
            cellLabel2.textAlignment = UITextAlignmentCenter;
            cellLabel2.textColor = [UIColor blackColor];
            cellLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            [cell.contentView addSubview:cellLabel2];
            
        }
        cellLabel1 = (UILabel *)[cell viewWithTag:1];
        cellLabel2 = (UILabel *)[cell viewWithTag:2];
        
        cell.contentView.backgroundColor = [UIColor whiteColor];
        
        if([category isEqualToString:ACTIVE])
        {
            NMActiveModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
            cellLabel1.text = tempObt.Feature;
            cellLabel2.text = tempObt.XE;
            if (showdiffBtn.isSelected && ([tempObt.Feature hasPrefix:@"Wheel Type"] || [tempObt.Feature hasPrefix:@"Tyre"])) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
            }
        }else if([category isEqualToString:PETROL]){
            NMPetrolModelObject *tempObt = (NMPetrolModelObject *)[objectArray objectAtIndex:indexPath.row];
            cellLabel1.text = tempObt.Feature;
            cellLabel2.text = tempObt.XL;
            
            if (showdiffBtn.isSelected && ([tempObt.Feature hasPrefix:@"Wheel Type"] || [tempObt.Feature hasPrefix:@"Tyre"])) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
            }
        }
        else {
            NMDieselModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
            cellLabel1.text = tempObt.Feature;
            cellLabel2.text = tempObt.XL;
            if (showdiffBtn.isSelected && ([tempObt.Feature hasPrefix:@"Wheel Type"] || [tempObt.Feature hasPrefix:@"Tyre"])) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
            }
        }
        return cell;

    } else
    {
        if ([category isEqualToString:PETROL]) {
            UILabel *cellLabel1;
            UILabel *cellLabel2;
            UILabel *cellLabel3;
            UILabel *cellLabel4;

            static NSString *CellIdentifier = @"tablediffpetrolcell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                DebugLog(@"id = %@",CellIdentifier);
                
                cellLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel1.backgroundColor = [UIColor clearColor];
                cellLabel1.frame = CGRectMake(2, 0, 140,45);
                cellLabel1.font = HELVETICA_FONT(12);
                cellLabel1.tag = 1;
                cellLabel1.numberOfLines = 2;
                cellLabel1.textColor = [UIColor blackColor];
                cellLabel1.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel1];
                
                cellLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel2.backgroundColor = [UIColor clearColor];
                cellLabel2.frame = CGRectMake(CGRectGetMaxX(cellLabel1.frame), 0, 60,45);
                cellLabel2.font = HELVETICA_FONT(12);
                cellLabel2.tag = 2;
                cellLabel2.numberOfLines = 2;
                cellLabel2.textAlignment = UITextAlignmentCenter;
                cellLabel2.textColor = [UIColor blackColor];
                cellLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel2];
                
                cellLabel3 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel3.backgroundColor = [UIColor clearColor];
                cellLabel3.frame = CGRectMake(CGRectGetMaxX(cellLabel2.frame), 0, 60,45);
                cellLabel3.font = HELVETICA_FONT(12);
                cellLabel3.tag = 3;
                cellLabel3.numberOfLines = 2;
                cellLabel3.textAlignment = UITextAlignmentCenter;
                cellLabel3.textColor = [UIColor blackColor];
                cellLabel3.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel3];
                
                cellLabel4 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel4.backgroundColor = [UIColor clearColor];
                cellLabel4.frame = CGRectMake(CGRectGetMaxX(cellLabel3.frame), 0, 60,45);
                cellLabel4.font = HELVETICA_FONT(12);
                cellLabel4.tag = 4;
                cellLabel4.numberOfLines = 2;
                cellLabel4.textAlignment = UITextAlignmentCenter;
                cellLabel4.textColor = [UIColor blackColor];
                cellLabel4.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel4];

            }
            cellLabel1 = (UILabel *)[cell viewWithTag:1];
            cellLabel2 = (UILabel *)[cell viewWithTag:2];
            cellLabel3 = (UILabel *)[cell viewWithTag:3];
            cellLabel4 = (UILabel *)[cell viewWithTag:4];
            
            if(showdiffBtn.isSelected)
                cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
            else
                cell.contentView.backgroundColor = [UIColor whiteColor];
            
            NMPetrolModelObject *tempObt = (NMPetrolModelObject *)[objectArray objectAtIndex:indexPath.row];
            DebugLog(@"cell-->>>>-%@-%@-%@-%@-",tempObt.Feature,tempObt.XL,tempObt.XL_O,tempObt.XV);
            
            cellLabel1.text = tempObt.Feature;
            cellLabel2.text = tempObt.XL;
            cellLabel3.text = tempObt.XL_O;
            cellLabel4.text = tempObt.XV;
            return cell;
        }else{ //Diesel or Active
            UILabel *cellLabel0;
            UILabel *cellLabel1;
            UILabel *cellLabel2;
            UILabel *cellLabel3;
            UILabel *cellLabel4;
            UILabel *cellLabel5;
            
            int width = 45;
            if([category isEqualToString:DIESEL])
            {
                width = 35;
            }
            
            static NSString *CellIdentifier = @"tablediffcell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                DebugLog(@"id = %@",CellIdentifier);
                
                cellLabel0 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel0.backgroundColor = [UIColor clearColor];
                cellLabel0.frame = CGRectMake(2, 0, 140,45);
                cellLabel0.font = HELVETICA_FONT(12);
                cellLabel0.tag = 1;
                cellLabel0.numberOfLines = 2;
                cellLabel0.textColor = [UIColor blackColor];
                cellLabel0.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel0];
                
                cellLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel1.backgroundColor = [UIColor clearColor];
                cellLabel1.frame = CGRectMake(CGRectGetMaxX(cellLabel0.frame)-3, 0, width,45);
                cellLabel1.font = HELVETICA_FONT(12);
                cellLabel1.tag = 2;
                cellLabel1.numberOfLines = 2;
                cellLabel1.textColor = [UIColor blackColor];
                cellLabel1.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel1];
                
                cellLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel2.backgroundColor = [UIColor clearColor];
                cellLabel2.frame = CGRectMake(CGRectGetMaxX(cellLabel1.frame)-3, 0, width,45);
                cellLabel2.font = HELVETICA_FONT(11);
                cellLabel2.tag = 3;
                cellLabel2.numberOfLines = 2;
                cellLabel2.textAlignment = UITextAlignmentCenter;
                cellLabel2.textColor = [UIColor blackColor];
                cellLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel2];
                
                cellLabel3 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel3.backgroundColor = [UIColor clearColor];
                cellLabel3.frame = CGRectMake(CGRectGetMaxX(cellLabel2.frame), 0, width,45);
                cellLabel3.font = HELVETICA_FONT(11);
                cellLabel3.tag = 4;
                cellLabel3.numberOfLines = 2;
                cellLabel3.textAlignment = UITextAlignmentCenter;
                cellLabel3.textColor = [UIColor blackColor];
                cellLabel3.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel3];
                
                cellLabel4 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel4.backgroundColor = [UIColor clearColor];
                cellLabel4.frame = CGRectMake(CGRectGetMaxX(cellLabel3.frame), 0, width,45);
                cellLabel4.font = HELVETICA_FONT(11);
                cellLabel4.tag = 5;
                cellLabel4.numberOfLines = 2;
                cellLabel4.textAlignment = UITextAlignmentCenter;
                cellLabel4.textColor = [UIColor blackColor];
                cellLabel4.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel4];
                
                cellLabel5 = [[UILabel alloc] initWithFrame:CGRectZero];
                cellLabel5.backgroundColor = [UIColor clearColor];
                cellLabel5.frame = CGRectMake(CGRectGetMaxX(cellLabel4.frame), 0, width,45);
                cellLabel5.font = HELVETICA_FONT(11);
                cellLabel5.tag = 6;
                cellLabel5.numberOfLines = 2;
                cellLabel5.textAlignment = UITextAlignmentCenter;
                cellLabel5.textColor = [UIColor blackColor];
                cellLabel5.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellLabel5];
            }
            cellLabel0 = (UILabel *)[cell viewWithTag:1];
            cellLabel1 = (UILabel *)[cell viewWithTag:2];
            cellLabel2 = (UILabel *)[cell viewWithTag:3];
            cellLabel3 = (UILabel *)[cell viewWithTag:4];
            cellLabel4 = (UILabel *)[cell viewWithTag:5];

            if(showdiffBtn.isSelected)
                cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
            else
                cell.contentView.backgroundColor = [UIColor whiteColor];
            
            if([category isEqualToString:ACTIVE])
            {
                NMActiveModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
                DebugLog(@"cell-->>>>-%@-%@-%@-%@-%@-",tempObt.Feature,tempObt.XE,tempObt.XL,tempObt.XV,tempObt.XV_S);
                cellLabel0.text = tempObt.Feature;
                cellLabel1.text = tempObt.XE;
                cellLabel2.text = tempObt.XL;
                cellLabel3.text = tempObt.XV;
                cellLabel4.text = tempObt.XV_S;
            }else{
                cellLabel5 = (UILabel *)[cell viewWithTag:6];
                NMDieselModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
                DebugLog(@"cell-->>>>-%@-%@-%@-%@-%@-",tempObt.Feature,tempObt.XL,tempObt.XL_O,tempObt.XV,tempObt.XV_Pre);
                cellLabel0.text = tempObt.Feature;
                cellLabel1.text = tempObt.XE;
                cellLabel2.text = tempObt.XL;
                cellLabel3.text = tempObt.XL_O;
                cellLabel4.text = tempObt.XV;
                cellLabel5.text = tempObt.XV_Pre;
            }
            return cell;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma Database Operations Methods
// ================= Features Table =================
-(void)loadObjectArray
{
    if (objectArray == nil) {
        objectArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [objectArray removeAllObjects];
    }
    NSString *queryString;
    if (sqlite3_open([[NM_APP_DELEGATE databasePath] UTF8String], &micraDB) == SQLITE_OK) {
        if([category isEqualToString:ACTIVE])
        {
            queryString = [NSString stringWithFormat:@"SELECT Models,XE ,XL ,XV ,XV_S FROM techspec_active"];
        } else if([category isEqualToString:PETROL])
        {
            queryString = [NSString stringWithFormat:@"SELECT Models,XL_Pet,XL_O_Pet,XV_CVT FROM techspec_micra"];
        } else
        {
            queryString = [NSString stringWithFormat:@"SELECT Models,XE_Dsl,XL_Dsl,XL_O_Dsl,XV_Dsl,XV_Premium_Dsl FROM techspec_micra"];
        }
        
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(micraDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                if([category isEqualToString:ACTIVE])
                {
                    NSString *featureString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *XEString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *XLString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *XVString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    NSString *XV_SString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 4)];
                    //DebugLog(@"%@%@%@%@%@",featureString,XEString,XLString,XVString,XV_SString);
                    NMActiveModelObject *tempObj = [[NMActiveModelObject alloc] init];
                    tempObj.Feature = featureString;
                    tempObj.XE = XEString;
                    tempObj.XL = XLString;
                    tempObj.XV = XVString;
                    tempObj.XV_S = XV_SString;
                    if([XEString isEqualToString:XLString] && [XEString isEqualToString:XVString] && [XEString isEqualToString:XV_SString])
                    {
                        
                        tempObj.is_same = TRUE;
                    } else
                    {
                        tempObj.is_same = FALSE;
                    }
                    
                    if (hidesimBtn.isSelected) {
                        if (!tempObj.is_same || ([tempObj.Feature hasPrefix:@"Wheel Type"] || [tempObj.Feature hasPrefix:@"Tyre"])) {
                            [objectArray addObject:tempObj];
                        }
                    }else{
                        [objectArray addObject:tempObj];
                    }
                } else if([category isEqualToString:PETROL])
                {
                    NSString *featureString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *XL_PetString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *XL_O_PetString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *XV_CVTString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    DebugLog(@"-%@-%@-%@-%@-",featureString,XL_PetString,XL_O_PetString,XV_CVTString);
                    NMPetrolModelObject *tempObj = [[NMPetrolModelObject alloc] init];
                    tempObj.Feature = featureString;
                    tempObj.XL = XL_PetString;
                    tempObj.XL_O = XL_O_PetString;
                    tempObj.XV = XV_CVTString;
                    if([XL_PetString isEqualToString:XL_O_PetString] && [XL_PetString isEqualToString:XV_CVTString])
                    {
                        
                        tempObj.is_same = TRUE;
                    } else
                    {
                        tempObj.is_same = FALSE;
                    }
                    if (hidesimBtn.isSelected) {
                        if (!tempObj.is_same || ([tempObj.Feature hasPrefix:@"Wheel Type"] || [tempObj.Feature hasPrefix:@"Tyre"])){
                            [objectArray addObject:tempObj];
                        }
                    }else{
                        [objectArray addObject:tempObj];
                    }
                    
                } else
                {
                    NSString *featureString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *XE_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *XL_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *XL_O_DsString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    NSString *XV_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 4)];
                    NSString *XV_Premium_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 5)];
                    
                    NMDieselModelObject *tempObj = [[NMDieselModelObject alloc] init];
                    tempObj.Feature = featureString;
                    tempObj.XE = XE_DslString;
                    tempObj.XL = XL_DslString;
                    tempObj.XL_O = XL_O_DsString;
                    tempObj.XV = XV_DslString;
                    tempObj.XV_Pre = XV_Premium_DslString;
                    if([XE_DslString isEqualToString:XL_DslString] && [XL_DslString isEqualToString:XL_O_DsString] && [XL_DslString isEqualToString:XV_DslString] && [XL_DslString isEqualToString:XV_Premium_DslString])
                    {
                        
                        tempObj.is_same = TRUE;
                    } else
                    {
                        tempObj.is_same = FALSE;
                    }
                    if (hidesimBtn.isSelected) {
                        if (!tempObj.is_same || ([tempObj.Feature hasPrefix:@"Wheel Type"] || [tempObj.Feature hasPrefix:@"Tyre"])) {
                            [objectArray addObject:tempObj];
                        }
                    }else{
                        [objectArray addObject:tempObj];
                    }
                }
             }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(micraDB);
    }
    [self.techspecsTableView reloadData];
}
#pragma Device Orientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTechspecsTableView:nil];
    [self setShowdiffBtn:nil];
    [self setHidesimBtn:nil];
    [self setModelPickerView:nil];
    [super viewDidUnload];
}
- (IBAction)showdiffBtnPressed:(UIButton*)sender {
    if (sender.isSelected) {
        sender.selected = FALSE;
        [sender setTitle:@"Show Differences" forState:UIControlStateNormal];
    }
    else
    {
        sender.selected = TRUE;
        [sender setTitle:@"Hide Differences" forState:UIControlStateNormal];
    }
    [techspecsTableView reloadData];
}

- (IBAction)hidesimBtnPressed:(UIButton*)sender {
    if (sender.isSelected) {
        sender.selected = FALSE;
        [sender setTitle:@"Hide Similarities" forState:UIControlStateNormal];
    }
    else
    {
        sender.selected = TRUE;
        [sender setTitle:@"Show Similarities" forState:UIControlStateNormal];
    }
    [self loadObjectArray];
    [techspecsTableView reloadData];
}
@end
