//
//  NMAppDelegate.h
//  NissanMicra
//
//  Created by Kirti Nikam on 18/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
#import "NMSplashViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <FacebookSDK/FacebookSDK.h>

#define NM_APP_DELEGATE (NMAppDelegate *)[[UIApplication sharedApplication] delegate]
extern NSString *const SCSessionStateChangedNotification;

@class GalleryGridViewController;
@class NMSplashViewController;

@interface NMAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
    
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) UIWindow *window;
@property(copy,nonatomic) NSString *databaseName;
@property(copy,nonatomic) NSString *databasePath;
@property (strong, nonatomic) NMSplashViewController *splashviewController;

-(void)animationStop;
- (NSString *) md5:(NSString *) input;
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname;
-(NSString *) getTextFromFile:(NSString *)lname;
- (void)removeFile:(NSString*)lname;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@end
