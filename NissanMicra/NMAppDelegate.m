//
//  NMAppDelegate.m
//  NissanMicra
//
//  Created by Kirti Nikam on 18/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMAppDelegate.h"
#import "GalleryGridViewController.h"
#import "MFSideMenu.h"
#import "SideMenuViewController.h"
#import "NMSplashViewController.h"
#import <Parse/Parse.h>
#import <CommonCrypto/CommonDigest.h>
#import <sqlite3.h>

//facebook
/*App ID/API Key
593390887359874
App secret
de48699e1008cde0d7094be3f3f324e0
*/
//#define FLURRY_APPID @"HRWPHQW8NZVW5YF4T5GN" //TEST APP Flurry ID
#define FLURRY_APPID @"F7XZ3F2YGY5GPRPFP64S"  //APP Flurry ID

#define PARSE_APPID @"Ug172uqQQtmM7vyBrUdpqveu1byCGsh23n5oBgRn" 
#define PARSE_CLIENTKEY @"83oXAMHuL0HFJPa1xrJMJ1Ol5ywNU4aNyMyty9ij" 

#define DATABASE_VERSION 1

NSString *const SCSessionStateChangedNotification = @"com.facebook.Neon:MSessionStateChangedNotification";

@implementation NMAppDelegate
@synthesize networkavailable;
@synthesize databaseName;
@synthesize databasePath;
@synthesize splashviewController;

#pragma iOS 6 orientation Methods
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskAll;
    DebugLog(@"self.window.rootViewController %@",self.window.rootViewController);
    if (self.window.rootViewController) {
        if ([self.window.rootViewController isKindOfClass:[NMSplashViewController class]]) {
            orientations = [self.window.rootViewController supportedInterfaceOrientations];
        }
        else{
            UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
            DebugLog(@"presented %@",presented);
            orientations = [presented supportedInterfaceOrientations];
            DebugLog(@"orientations %lu",(unsigned long)orientations);
            if (orientations == UIInterfaceOrientationMaskPortrait) {
                DebugLog(@"UIInterfaceOrientationMaskPortrait");
            }
            else  if (orientations == UIInterfaceOrientationMaskLandscapeLeft) {
                DebugLog(@"UIInterfaceOrientationMaskLandscapeLeft");
            }
            else  if (orientations == UIInterfaceOrientationMaskLandscapeRight) {
                DebugLog(@"UIInterfaceOrientationMaskLandscapeRight");
            }
            else  if (orientations == UIInterfaceOrientationMaskPortraitUpsideDown) {
                DebugLog(@"UIInterfaceOrientationMaskPortraitUpsideDown");
            }
            else  if (orientations == UIInterfaceOrientationMaskAll) {
                DebugLog(@"UIInterfaceOrientationMaskAll");
            }
            else  if (orientations == UIInterfaceOrientationMaskLandscape) {
                DebugLog(@"UIInterfaceOrientationMaskLandscape");
            }
            else  if (orientations == UIInterfaceOrientationMaskAllButUpsideDown) {
                DebugLog(@"UIInterfaceOrientationMaskAllButUpsideDown");
            }
        }
    }
    return orientations;
}

#pragma To add sideMenu Methods
- (GalleryGridViewController *)giaController {
    return [[GalleryGridViewController alloc] initWithNibName:@"GalleryGridViewController" bundle:nil];
}

- (UINavigationController *)navigationController {
    return [[UINavigationController alloc]
            initWithRootViewController:[self giaController]];
}

- (MFSideMenu *)sideMenu
{
    SideMenuViewController *rightSideMenuController = [[SideMenuViewController alloc] init];
    UINavigationController *navigationController = [self navigationController];
    navigationController.navigationBar.tintColor = DEFAULT_COLOR;
//    navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
//    navigationController.navigationBar.layer.shadowOpacity = 1;
//    navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0,4);
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
                                             leftSideMenuController:nil
                                            rightSideMenuController:rightSideMenuController];
    
    rightSideMenuController.sideMenu = sideMenu;
    
    return sideMenu;
}

- (void) setupNavigationControllerApp {
    // self.sideMenu.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.window.rootViewController = [self sideMenu].navigationController;
    splashviewController = nil;
    [self.window makeKeyAndVisible];
}

-(void)animationStop
{
    //[splashviewController.view removeFromSuperview];
    //splashviewController = nil;
    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self setupNavigationControllerApp];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    //Add Reachability
    [self startCheckNetwork];
    
    //Create Database
    [self readAndCreateDatabase];
    
    //Add SlideMenu
    //[self setupNavigationControllerApp];
    
    //Add Parse
    [Parse setApplicationId:PARSE_APPID clientKey:PARSE_CLIENTKEY];
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
    //Add Flurry
    [Flurry startSession:FLURRY_APPID];
    [Flurry logEvent:@"Nissan Micra App Started"];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
                          UITextAttributeTextShadowColor: [UIColor blackColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: HELVETICA_FONT(18.0)
     }];
    
//    [[UINavigationBar appearance] setBackgroundImage:[self blueBarBackground] forBarMetrics:UIBarMetricsDefault];
    
   
//    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
//                                UITextAttributeTextColor: [UIColor whiteColor],
//                          UITextAttributeTextShadowColor: [UIColor blackColor],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
//                                     UITextAttributeFont: HELVETICA_FONT(15.0)
//     } forState:UIControlStateNormal];
//    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
//                                UITextAttributeTextColor: [UIColor whiteColor],
//                          UITextAttributeTextShadowColor: [UIColor blackColor],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
//                                     UITextAttributeFont: HELVETICA_FONT(15.0)
//     } forState:UIControlStateHighlighted];
    
    splashviewController = [[NMSplashViewController alloc] initWithNibName:@"NMSplashViewController" bundle:nil];
    
    self.window.rootViewController = splashviewController;
    
    //[self.window addSubview:splashviewController.view];
    
    [self.window makeKeyAndVisible];
    
    
    NSArray *fonts = [UIFont familyNames];

    for(NSString *string in fonts){
        DebugLog(@"%@", string);
    }
    
    return YES;
}
- (UIImage *)blueBarBackground
{
    /* Create a DeviceRGB color space. */
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    /* Create a bitmap context. The context draws into a bitmap which is `width'
     pixels wide and `height' pixels high*/
    CGContextRef composedImageContext = CGBitmapContextCreate(NULL,
                                                              10,
                                                              10,
                                                              8,
                                                              10*4,
                                                              colorSpace,
                                                              kCGImageAlphaPremultipliedFirst);
    
    CGColorSpaceRelease(colorSpace);
    
    
    CGContextSetFillColorWithColor(composedImageContext, DEFAULT_COLOR.CGColor);
    CGContextFillRect(composedImageContext, CGRectMake(0, 0, 10, 10));
    /* Return an image containing a snapshot of the bitmap context `context'.*/
    CGImageRef cgImage = CGBitmapContextCreateImage(composedImageContext);
    
    return [UIImage imageWithCGImage:cgImage];
}
#pragma Push Notification CallBacks
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //DebugLog(@"didRegisterForRemoteNotificationsWithDeviceToken : devicetoken %@",deviceToken);
    [PFPush storeDeviceToken:deviceToken];
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            DebugLog(@"Successfully subscribed to broadcast channel!");
        else
            DebugLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DebugLog(@"didFailToRegisterForRemoteNotificationsWithError");
    if (error.code == 3010) {
        DebugLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        DebugLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DebugLog(@"didReceiveRemoteNotification  : userInfo %@",userInfo);
    [PFPush handlePush:userInfo];
	for (NSString *key in [userInfo allKeys]) {
		DebugLog(@"%@ is %@",key, [userInfo objectForKey:key]);
	}
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
        DebugLog(@"UIApplicationStateInactive");
    } else if (state == UIApplicationStateBackground){
        DebugLog(@"UIApplicationStateBackground");
    } else if (state == UIApplicationStateActive){
        DebugLog(@"UIApplicationStateActive");
    }
}


#pragma Database Methods
-(void)readAndCreateDatabase{
    // Setup some globals
	databaseName = @"micra.db";
    
	// Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
    
    DebugLog(@"databasePath %@",databasePath);
	// Execute the "checkAndCreateDatabase" function
	[self checkAndCreateDatabase];
}

-(void) checkAndCreateDatabase{
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
    
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
    
	// If the database already exists then return without doing anything
//	if(success) return;
    if (success) {
        int oldVersion = [self checkdatabaseversion:databasePath];
        DebugLog(@"old version %d",oldVersion);
        DebugLog(@"DATABASE_VERSION  %d",DATABASE_VERSION);
        
        if (oldVersion < DATABASE_VERSION) {
            DebugLog(@"old version != DATABASE_VERSION");
            [fileManager removeItemAtPath:databasePath error:NULL];
           
        }else{
            DebugLog(@"old version == DATABASE_VERSION");
            return;
        }
    }
    
	// If not then proceed to copy the database from the application to the users filesystem
    
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    
	// Copy the database from the package to the users filesystem
	BOOL result = [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
    if(result)
    {
        DebugLog(@"Database created");
        [self setDatabaseSchemaVersion:DATABASE_VERSION];
    }
}

-(int)checkdatabaseversion:(NSString *)checkdbpath
{
    sqlite3 *micraDB;
    static sqlite3_stmt *stmt_version;
    int databaseVersion = 0;
    
    const char *dbpath = [checkdbpath UTF8String];
    
    if (sqlite3_open(dbpath, &micraDB) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(micraDB, "PRAGMA user_version;", -1, &stmt_version, NULL) == SQLITE_OK) {
            while(sqlite3_step(stmt_version) == SQLITE_ROW) {
                databaseVersion = sqlite3_column_int(stmt_version, 0);
                DebugLog(@"%s: version %d", __FUNCTION__, databaseVersion);
            }
            DebugLog(@"%s: the databaseVersion is: %d", __FUNCTION__, databaseVersion);
        } else {
            DebugLog(@"%s: ERROR Preparing: , %s", __FUNCTION__, sqlite3_errmsg(micraDB) );
        }
        sqlite3_finalize(stmt_version);
    }
    sqlite3_close(micraDB);
    
    return databaseVersion;
}

- (void)setDatabaseSchemaVersion:(int)version {
    sqlite3 *micraDB;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &micraDB) == SQLITE_OK)
    {
        sqlite3_exec(micraDB, [[NSString stringWithFormat:@"PRAGMA user_version = %d", version] UTF8String], NULL, NULL, NULL);
    }
}

#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
    [self sendNetworkNotification];
}

#pragma Wifi Notification CallBack
-(void)sendNetworkNotification
{
    if (networkavailable) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NETWORK_NOTIFICATION object:nil];
    }
}

- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}
#pragma File handling callbacks
//Method writes a string to a text file
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    // DebugLog(@"fullpath = %@" , fullPath);
    
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        [self removeFile:fileName];
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    } else {
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    }
    //    if(error != nil)
    //        DebugLog(@"write error %@", error);
    //    else {
    //        DebugLog(@"file created %@", fileName);
    //    }
    
	
}

-(NSString *) getTextFromFile:(NSString *)lname
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    //get the documents directory:
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	//make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    DebugLog(@"fullpath = %@" , fullPath);
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSString *data = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
        //        if(error != nil)
        //        {
        //            DebugLog(@"read error %@", error);
        //            return @"";
        //        }
        return data;
    } else {
        return @"";
    }
}
- (void)removeFile:(NSString*)lname {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:lname];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES)
        DebugLog(@" %@ removed",lname);
    else
        DebugLog(@" %@ NOT removed",lname);
}

#pragma Facbook callBack Methods
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            DebugLog(@"mytoken=%@",[FBSession.activeSession accessToken]);
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
        {
            // FBSample logic
            // Once the user has logged in, we want them to be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"" forKey:@"userId"];
            [defaults setObject:@"" forKey:@"accessToken"];
            [defaults synchronize];
            //[self showLoginView];
        }
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SCSessionStateChangedNotification
                                                        object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions", @"user_photos", nil];
    return [FBSession openActiveSessionWithPermissions:permissions
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                         [self sessionStateChanged:session state:state error:error];
                                     }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    return [FBSession.activeSession handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession close];
}
@end
