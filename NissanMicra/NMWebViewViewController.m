//
//  NMWebViewViewController.m
//  NissanMicra
//
//  Created by Rishi on 20/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMWebViewViewController.h"
#import "MBProgressHUD.h"
#import "constants.h"

#define WEB_LINK @"https://www.facebook.com/nissanindia"

@interface NMWebViewViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;

@end

@implementation NMWebViewViewController
@synthesize webView;
@synthesize barbackBtn;
@synthesize barfwdBtn;
@synthesize barrefreshBtn;
@synthesize webviewtoolBar;
@synthesize progressHUD;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Facebook_Tab_Event"];

    barbackBtn.enabled      = NO;
    barfwdBtn.enabled       = NO;
    barrefreshBtn.enabled   = NO;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:WEB_LINK]];
    [webView loadRequest:requestObj];
}
- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
#pragma mark webView Delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showProgressHUDWithMessage:@"Loading"];
    barrefreshBtn.enabled   = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (self.webView.canGoBack)
    {
        barbackBtn.enabled = YES;
    }
    else
    {
        barbackBtn.enabled = NO;
    }
    if (self.webView.canGoForward)
    {
        barfwdBtn.enabled = YES;
    }
    else
    {
        barfwdBtn.enabled = NO;
    }
    barrefreshBtn.enabled = YES;
    [self hideProgressHUD:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    barrefreshBtn.enabled = YES;
    [self hideProgressHUD:YES];
    
}
- (IBAction)webviewbackBtnPressed:(id)sender {
    [webView goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [webView goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [webView reload];
}
#pragma Device Orientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setBarbackBtn:nil];
    [self setBarfwdBtn:nil];
    [self setBarrefreshBtn:nil];
    [self setWebviewtoolBar:nil];
    [super viewDidUnload];
}
@end
