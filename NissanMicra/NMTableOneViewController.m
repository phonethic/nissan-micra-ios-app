//
//  NMTableOneViewController.m
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "NMTableOneViewController.h"
#import "NMAppDelegate.h"
#import "NMActiveModelObject.h"
#import "NMDieselModelObject.h"
#import "NMPetrolModelObject.h"

//@interface UILabel (MultiLineAutoSize)
//- (void)adjustFontSizeToFit;
//@end
//@implementation UILabel (MultiLineAutoSize)
//- (void)adjustFontSizeToFit
//{
//    UIFont *font = self.font;
//    CGSize size = self.frame.size;
//    
//    for (CGFloat maxSize = self.font.pointSize; maxSize >= self.minimumFontSize; maxSize -= 1.f)
//    {
//        font = [font fontWithSize:maxSize];
//        CGSize constraintSize = CGSizeMake(size.width, MAXFLOAT);
//        CGSize labelSize = [self.text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
//        if(labelSize.height <= size.height)
//        {
//            self.font = font;
//            [self setNeedsLayout];
//            break;
//        }
//    }
//    // set the font to the minimum size anyway
//    self.font = font;
//    [self setNeedsLayout];
//}
//@end

@interface NMTableOneViewController ()

@end

@implementation NMTableOneViewController
@synthesize selectedHeader;
@synthesize category;
@synthesize objectArray;
@synthesize tableOneView;
@synthesize showdiffSelected,hideSimSelected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableOneView.backgroundColor = BACKGROUD_COLOR;
    DebugLog(@"--%d--%d--",showdiffSelected,hideSimSelected);
    [self loadObjectArray];
}

#pragma mark Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    DebugLog(@"W==%f H==%f",self.view.frame.size.width,self.view.frame.size.height);
    int width = 0;
    if([category isEqualToString:ACTIVE])
    {
        width = 15;
    }else if([category isEqualToString:DIESEL])
    {
        width = 25;
    }
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,45)];
    customView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel0 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel0.backgroundColor = [UIColor clearColor];
    headerLabel0.frame = CGRectMake(0, 0, 140, 45);
    headerLabel0.font = HELVETICA_FONT(12);
    headerLabel0.text = @"Features";
    headerLabel0.textAlignment = UITextAlignmentCenter;
    headerLabel0.layer.borderWidth = 1.0;
    headerLabel0.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel0.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel0.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel0];
    
    UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel1.backgroundColor = [UIColor clearColor];
    headerLabel1.frame = CGRectMake(CGRectGetMaxX(headerLabel0.frame), 0, 60-width, 45);
    headerLabel1.font = HELVETICA_FONT(12);
    if([category isEqualToString:PETROL])
        headerLabel1.text = @"XL";
    else
        headerLabel1.text = @"XE";
    headerLabel1.textAlignment = UITextAlignmentCenter;
    headerLabel1.layer.borderWidth = 1.0;
    headerLabel1.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel1.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel1.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel1];
    
    UILabel *headerLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel2.backgroundColor = [UIColor clearColor];
    headerLabel2.frame = CGRectMake(CGRectGetMaxX(headerLabel1.frame), 0, 60-width, 45);
    headerLabel2.font = HELVETICA_FONT(12);
    if([category isEqualToString:PETROL])
        headerLabel2.text = @"XL(0)";
    else
        headerLabel2.text = @"XL";
    headerLabel2.textAlignment = UITextAlignmentCenter;
    headerLabel2.layer.borderWidth = 1.0;
    headerLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel2.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel2.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel2];
    
    UILabel *headerLabel3 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel3.backgroundColor = [UIColor clearColor];
    headerLabel3.frame = CGRectMake(CGRectGetMaxX(headerLabel2.frame), 0, 60-width, 45);
    headerLabel3.font = HELVETICA_FONT(12);
    if([category isEqualToString:PETROL])
        headerLabel3.text = @"XV CVT";
    else if([category isEqualToString:DIESEL])
        headerLabel3.text = @"XL(0)";
    else
        headerLabel3.text = @"XV";
    headerLabel3.textAlignment = UITextAlignmentCenter;
    headerLabel3.layer.borderWidth = 1.0;
    headerLabel3.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel3.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel3.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel3];
    
    if([category isEqualToString:ACTIVE] || [category isEqualToString:DIESEL])
    {
        UILabel *headerLabel4 = [[UILabel alloc] initWithFrame:CGRectZero];
        headerLabel4.backgroundColor = [UIColor clearColor];
        headerLabel4.frame = CGRectMake(CGRectGetMaxX(headerLabel3.frame), 0, 60-width, 45);
        headerLabel4.font = HELVETICA_FONT(12);
        if([category isEqualToString:DIESEL])
            headerLabel4.text = @"XV";
        else
            headerLabel4.text = @"XV(S)";
        headerLabel4.textAlignment = UITextAlignmentCenter;
        headerLabel4.layer.borderWidth = 1.0;
        headerLabel4.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        headerLabel4.layer.borderColor = [UIColor blackColor].CGColor;
        headerLabel4.textColor = [UIColor blackColor];
        [customView addSubview:headerLabel4];
        
        if([category isEqualToString:DIESEL])
        {
            UILabel *headerLabel5 = [[UILabel alloc] initWithFrame:CGRectZero];
            headerLabel5.backgroundColor = [UIColor clearColor];
            headerLabel5.frame = CGRectMake(CGRectGetMaxX(headerLabel4.frame), 0, 60-width+5, 45);
            headerLabel5.font = HELVETICA_FONT(12);
            if([category isEqualToString:DIESEL])
                headerLabel5.text = @"XV(P)";
            
            headerLabel5.textAlignment = UITextAlignmentCenter;
            headerLabel5.layer.borderWidth = 1.0;
            headerLabel5.layer.borderColor = [UIColor blackColor].CGColor;
            headerLabel5.textColor = [UIColor blackColor];
            headerLabel5.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            [customView addSubview:headerLabel5];
        }
    }
    return customView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [objectArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 69;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UILabel *cellLabel1;
    UIImageView *cellImageView1;
    UIImageView *cellImageView2;
    UIImageView *cellImageView3;
    UIImageView *cellImageView4;
    UIImageView *cellImageView5;
    
    int width = 0;
    
    if([category isEqualToString:ACTIVE])
    {
        width = 15;
    }else if([category isEqualToString:DIESEL])
    {
        width = 25;
    }
    static NSString *CellIdentifier = @"tableonecell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth  | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        cellLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
        cellLabel1.backgroundColor = [UIColor clearColor];
        cellLabel1.frame = CGRectMake(0, 2, 140, 65);
        cellLabel1.font = HELVETICA_FONT(12);
        cellLabel1.tag = 1;
        cellLabel1.numberOfLines = 0;
        cellLabel1.textColor = [UIColor blackColor];
        cellLabel1.autoresizingMask = UIViewAutoresizingFlexibleWidth  | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [cell.contentView addSubview:cellLabel1];

        cellImageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellLabel1.frame) ,0, 60-width, 50)];
        cellImageView1.contentMode = UIViewContentModeScaleAspectFit;
        cellImageView1.backgroundColor = [UIColor clearColor];
        cellImageView1.tag = 2;
        cellImageView1.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin |  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [cell.contentView addSubview:cellImageView1];
        
        cellImageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView1.frame) ,0, 60-width, 50)];
        cellImageView2.contentMode = UIViewContentModeScaleAspectFit;
        cellImageView2.backgroundColor = [UIColor clearColor];
        cellImageView2.tag = 3;
        cellImageView2.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin |  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [cell.contentView addSubview:cellImageView2];
        
        cellImageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView2.frame) ,0, 60-width, 50)];
        cellImageView3.contentMode = UIViewContentModeScaleAspectFit;
        cellImageView3.backgroundColor = [UIColor clearColor];
        cellImageView3.tag = 4;
        cellImageView3.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin |  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [cell.contentView addSubview:cellImageView3];
        
        if([category isEqualToString:ACTIVE] || [category isEqualToString:DIESEL])
        {
            cellImageView4 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView3.frame)  ,0, 60-width, 50)];
            cellImageView4.contentMode = UIViewContentModeScaleAspectFit;
            cellImageView4.backgroundColor = [UIColor clearColor];
            cellImageView4.tag = 5;
            cellImageView4.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin |  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            [cell.contentView addSubview:cellImageView4];
        
            if([category isEqualToString:DIESEL])
            {
                cellImageView5 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView4.frame) ,0, 60-width, 50)];
                cellImageView5.contentMode = UIViewContentModeScaleAspectFit;
                cellImageView5.backgroundColor = [UIColor clearColor];
                cellImageView5.tag = 6;
                cellImageView5.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell.contentView addSubview:cellImageView5];
            }
        }
    }
    cellLabel1 = (UILabel *)[cell viewWithTag:1];
    cellImageView1 = (UIImageView *)[cell viewWithTag:2];
    cellImageView2 = (UIImageView *)[cell viewWithTag:3];
    cellImageView3 = (UIImageView *)[cell viewWithTag:4];
    
    if([category isEqualToString:ACTIVE])
    {
        cellImageView4 = (UIImageView *)[cell viewWithTag:5];
        NMActiveModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
        cellLabel1.text = tempObt.Feature;
        [self setImageFromText:tempObt.XE imageView:cellImageView1];
        [self setImageFromText:tempObt.XL imageView:cellImageView2];
        [self setImageFromText:tempObt.XV imageView:cellImageView3];
        [self setImageFromText:tempObt.XV_S imageView:cellImageView4];
        
        if(showdiffSelected && !tempObt.is_same)
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
        else
            cell.contentView.backgroundColor = [UIColor whiteColor];

        
    }else if([category isEqualToString:PETROL]){
        NMPetrolModelObject *tempObt = (NMPetrolModelObject *)[objectArray objectAtIndex:indexPath.row];
        //DebugLog(@"cell-->>>>-%@-%@-%@-%@-",tempObt.Feature,tempObt.XL,tempObt.XL_O,tempObt.XV);
        cellLabel1.text = tempObt.Feature;
        [self setImageFromText:tempObt.XL imageView:cellImageView1];
        [self setImageFromText:tempObt.XL_O imageView:cellImageView2];
        [self setImageFromText:tempObt.XV imageView:cellImageView3];
        
        if(showdiffSelected && !tempObt.is_same)
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
        else
            cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    else {
        cellImageView4 = (UIImageView *)[cell viewWithTag:5];
        cellImageView5 = (UIImageView *)[cell viewWithTag:6];
        NMDieselModelObject *tempObt = [objectArray objectAtIndex:indexPath.row];
        cellLabel1.text = tempObt.Feature;
        [self setImageFromText:tempObt.XE imageView:cellImageView1];
        [self setImageFromText:tempObt.XL imageView:cellImageView2];
        [self setImageFromText:tempObt.XL_O imageView:cellImageView3];
        [self setImageFromText:tempObt.XV imageView:cellImageView4];
        [self setImageFromText:tempObt.XV_Pre imageView:cellImageView5];
        
        if(showdiffSelected && !tempObt.is_same)
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.505 green:0.854 blue:0.960 alpha:0.8];
        else
            cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
 //   [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma Database Operations Methods
// ================= Features Table =================
-(void)loadObjectArray
{
    DebugLog(@"selectedHeader=%@",selectedHeader);
    if (objectArray == nil) {
        objectArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [objectArray removeAllObjects];
    }
    NSString *queryString;
    if (sqlite3_open([[NM_APP_DELEGATE databasePath] UTF8String], &micraDB) == SQLITE_OK) {
        if([category isEqualToString:ACTIVE])
        {
            queryString = [NSString stringWithFormat:@"SELECT Features ,XE ,XL ,XV ,XV_S FROM features_active where Headers = '%@'",selectedHeader];
        } else if([category isEqualToString:PETROL])
        {
            queryString = [NSString stringWithFormat:@"SELECT Features,XL_Pet,XL_O_Pet,XV_CVT FROM features_micra where Headers = '%@'",selectedHeader];
        } else
        {
            queryString = [NSString stringWithFormat:@"SELECT Features,XE_Dsl,XL_Dsl,XL_O_Dsl,XV_Dsl,XV_Premium_Dsl FROM features_micra where Headers = '%@'",selectedHeader];
        }
        
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(micraDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                if([category isEqualToString:ACTIVE])
                {
                    NSString *featureString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *XEString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *XLString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *XVString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    NSString *XV_SString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 4)];
                    //DebugLog(@"%@%@%@%@%@",featureString,XEString,XLString,XVString,XV_SString);
                    NMActiveModelObject *tempObj = [[NMActiveModelObject alloc] init];
                    tempObj.Feature = featureString;
                    tempObj.XE = XEString;
                    tempObj.XL = XLString;
                    tempObj.XV = XVString;
                    tempObj.XV_S = XV_SString;
                    
                    if([XEString isEqualToString:XLString] && [XEString isEqualToString:XVString] && [XEString isEqualToString:XV_SString])
                    {
                        
                        tempObj.is_same = TRUE;
                    } else
                    {
                        tempObj.is_same = FALSE;
                    }
                    
                    if (hideSimSelected) {
                        if (!tempObj.is_same) {
                            [objectArray addObject:tempObj];
                        }
                    }else{
                        [objectArray addObject:tempObj];
                    }
                    
                } else if([category isEqualToString:PETROL])
                {
                    NSString *featureString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *XL_PetString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *XL_O_PetString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *XV_CVTString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    DebugLog(@"-%@-%@-%@-%@-",featureString,XL_PetString,XL_O_PetString,XV_CVTString);
                    NMPetrolModelObject *tempObj = [[NMPetrolModelObject alloc] init];
                    tempObj.Feature = featureString;
                    tempObj.XL = XL_PetString;
                    tempObj.XL_O = XL_O_PetString;
                    tempObj.XV = XV_CVTString;
                    if([XL_PetString isEqualToString:XL_O_PetString] && [XL_PetString isEqualToString:XV_CVTString])
                    {
                        
                        tempObj.is_same = TRUE;
                    } else
                    {
                        tempObj.is_same = FALSE;
                    }
                    if (hideSimSelected) {
                        if (!tempObj.is_same) {
                            [objectArray addObject:tempObj];
                        }
                    }else{
                        [objectArray addObject:tempObj];
                    }

                    
                } else
                {
                    NSString *featureString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *XE_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *XL_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *XL_O_DsString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    NSString *XV_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 4)];
                    NSString *XV_Premium_DslString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 5)];
                    
                    NMDieselModelObject *tempObj = [[NMDieselModelObject alloc] init];
                    tempObj.Feature = featureString;
                    tempObj.XE = XE_DslString;
                    tempObj.XL = XL_DslString;
                    tempObj.XL_O = XL_O_DsString;
                    tempObj.XV = XV_DslString;
                    tempObj.XV_Pre = XV_Premium_DslString;
                    if([XE_DslString isEqualToString:XL_DslString] && [XL_DslString isEqualToString:XL_O_DsString] && [XL_DslString isEqualToString:XV_DslString] && [XL_DslString isEqualToString:XV_Premium_DslString])
                    {
                        
                        tempObj.is_same = TRUE;
                    } else
                    {
                        tempObj.is_same = FALSE;
                    }
                    if (hideSimSelected) {
                        if (!tempObj.is_same) {
                            [objectArray addObject:tempObj];
                        }
                    }else{
                        [objectArray addObject:tempObj];
                    }
                }
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(micraDB);
    }
    [self.tableOneView reloadData];
}

#pragma Image Set Operation Method
-(void)setImageFromText:(NSString *)text imageView:(UIImageView *)cellImageView
{
    if([text isEqualToString:@"s"] || [text isEqualToString:@"S"])
    {
        cellImageView.image = [UIImage imageNamed:@"bullet_tick.png"];
    } else {
        cellImageView.image = [UIImage imageNamed:@"cancel.png"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableOneView:nil];
    [super viewDidUnload];
}
@end
