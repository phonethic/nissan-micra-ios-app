//
//  NMActiveModelObject.h
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMActiveModelObject : NSObject
@property (nonatomic, copy) NSString *Feature;
@property (nonatomic, copy) NSString *XE;
@property (nonatomic, copy) NSString *XL;
@property (nonatomic, copy) NSString *XV;
@property (nonatomic, copy) NSString *XV_S;
@property (nonatomic, readwrite) BOOL is_same;
@end
