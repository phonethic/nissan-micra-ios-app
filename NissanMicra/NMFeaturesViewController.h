//
//  NMFeaturesViewController.h
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSlidingPagesDataSource.h"
#import <sqlite3.h>

@interface NMFeaturesViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate,TTSlidingPagesDataSource>
{
    UIActionSheet *actionSheet;
    TTScrollSlidingPagesController *slider;
    sqlite3 *micraDB;
}

@property (strong, nonatomic) NSMutableArray *modelObjectsArray;
@property (strong, nonatomic) UIPickerView *modelPickerView;
@property (copy,nonatomic) NSString *selectedCategory;
@property (strong, nonatomic) NSMutableArray *headersArray;
@property (strong, nonatomic) IBOutlet UIButton *showdiffBtn;
@property (strong, nonatomic) IBOutlet UIButton *hidesimBtn;

- (IBAction)showdiffBtnPressed:(id)sender;
- (IBAction)hidesimBtnPressed:(id)sender;
@end
