//
//  NMFeaturesViewController.m
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMFeaturesViewController.h"
#import "MFSideMenu.h"
#import "TTUIScrollViewSlidingPages.h"
#import "TTSlidingPage.h"
#import "TTSlidingPageTitle.h"
#import "NMTableOneViewController.h"
#import "NMAppDelegate.h"
#import "constants.h"



@interface NMFeaturesViewController ()

@end

@implementation NMFeaturesViewController
@synthesize modelPickerView;
@synthesize modelObjectsArray;
@synthesize selectedCategory;
@synthesize headersArray;
@synthesize showdiffBtn,hidesimBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = BACKGROUD_COLOR;

    [self setupMenuBarButtonItems];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"MODEL" style:UIBarButtonSystemItemDone target:self action:@selector(model_Clicked:)];
    
    
    [self changeButtonFontAndTextColor:showdiffBtn];
    [self changeButtonFontAndTextColor:hidesimBtn];
    [showdiffBtn setTitle:@"Show Differences" forState:UIControlStateNormal];
    [hidesimBtn setTitle:@"Hide Similarities" forState:UIControlStateNormal];
    
    modelObjectsArray = [[NSMutableArray alloc] init];
    [modelObjectsArray addObject:PETROL];
    [modelObjectsArray addObject:DIESEL];
    [modelObjectsArray addObject:ACTIVE];
    
    headersArray = [[NSMutableArray alloc] init];
    
    selectedCategory = [modelObjectsArray objectAtIndex:0];
    self.navigationItem.leftBarButtonItem.title = selectedCategory;
    
    [self loadHeadersArray];
    
    [self addPickerWithDoneButton]; //Adding picker for choosing Car models
    
    slider = [[TTScrollSlidingPagesController alloc] init];
    slider.disableUIPageControl = YES;
    slider.titleScrollerBackgroundColour = [UIColor colorWithWhite:0.901 alpha:1.0];//[UIColor colorWithRed:0.901 green:0.901 blue:0.901 alpha:1.0];
    slider.titleScrollerTextColour = [UIColor darkGrayColor];
    slider.disableTitleScrollerShadow = YES;
    slider.minimumZoom = 0.93;
    slider.dataSource = self; /*the current view controller (self) conforms to the TTSlidingPagesDataSource protocol)*/
    slider.view.frame = self.view.frame; //I'm setting up the view to be fullscreen in the current view
    [self.view addSubview:slider.view];
    [self addChildViewController:slider];
    
    [self.view bringSubviewToFront:showdiffBtn];
    [self.view bringSubviewToFront:hidesimBtn];

    DebugLog(@"selectedCategory -%@-",selectedCategory);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:selectedCategory,@"Model",nil];
    [Flurry logEvent:@"Features_Tab_Event" withParameters:params];
}
-(void)changeButtonFontAndTextColor:(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:TABLE_SELECTION_COLOR forState:UIControlStateHighlighted];
    [btn setBackgroundColor:[UIColor grayColor]];
    btn.titleLabel.font = HELVETICA_FONT(14);
    btn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
    btn.layer.borderWidth = 2.0;
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Bar Button method
- (void) model_Clicked:(id)sender {
    UISegmentedControl *doneButton = (UISegmentedControl *)[actionSheet viewWithTag:1000];
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    modelPickerView.frame = CGRectMake(modelPickerView.frame.origin.x,modelPickerView.frame.origin.y,self.view.frame.size.width,216);
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, self.view.frame.size.width,485)];
}


#pragma Model Picker method
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    modelPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    modelPickerView.showsSelectionIndicator = YES;
    modelPickerView.dataSource = self;
    modelPickerView.delegate = self;
    [actionSheet addSubview:modelPickerView];
    
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    doneButton.tag = 1000;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    
    [modelPickerView reloadAllComponents];
    if([modelPickerView numberOfRowsInComponent:0] != -1 && modelObjectsArray.count > 0)
    {
        [modelPickerView selectRow:0 inComponent:0 animated:YES];
        selectedCategory = [modelObjectsArray objectAtIndex:0];
    }
}

#pragma mark Picker Button method
- (void)doneFilterBtnClicked:(id)sender
{
    selectedCategory = [modelObjectsArray objectAtIndex:[modelPickerView selectedRowInComponent:0]];
    self.navigationItem.leftBarButtonItem.title = selectedCategory;
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    [self loadHeadersArray];
    
    DebugLog(@"selectedCategory -%@-",selectedCategory);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:selectedCategory,@"Model",nil];
    [Flurry logEvent:@"Features_Tab_Event" withParameters:params];
}

- (void)cancelFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [modelObjectsArray count];
}

//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
//{
//    return self.view.frame.size.width;
//}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    //DebugLog(@"titleForRow: categorypickerarray %@",categoryObjectsArray);
    return [modelObjectsArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{


}

#pragma mark TTSlidingPagesDataSource methods
-(int)numberOfPagesForSlidingPagesViewController:(TTScrollSlidingPagesController *)source{
    DebugLog(@"count=======%d",headersArray.count);
    return [headersArray count];
}

-(TTSlidingPage *)pageForSlidingPagesViewController:(TTScrollSlidingPagesController*)source atIndex:(int)index{
    NMTableOneViewController *viewController;
    viewController = [[NMTableOneViewController alloc] init];
    DebugLog(@"1)  selectedHeader=%@",[headersArray objectAtIndex:slider.getCurrentDisplayedPage]);
    viewController.category = selectedCategory;
    viewController.selectedHeader = [headersArray objectAtIndex:index];
    viewController.showdiffSelected = showdiffBtn.isSelected;
    viewController.hideSimSelected = hidesimBtn.isSelected;

    return [[TTSlidingPage alloc] initWithContentViewController:viewController];
}

-(TTSlidingPageTitle *)titleForSlidingPagesViewController:(TTScrollSlidingPagesController *)source atIndex:(int)index{
    TTSlidingPageTitle *title = [[TTSlidingPageTitle alloc] initWithHeaderText:[headersArray objectAtIndex:index]];
    return title;
}

#pragma Database Operations Methods
// ================= Features Table =================
-(void)loadHeadersArray
{
    if (headersArray == nil) {
        headersArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [headersArray removeAllObjects];
    }
    
    NSString *tempString;
    NSString *queryString;
    if (sqlite3_open([[NM_APP_DELEGATE databasePath] UTF8String], &micraDB) == SQLITE_OK) {
        if([selectedCategory isEqualToString:ACTIVE])
        {
            queryString = [NSString stringWithFormat:@"SELECT DISTINCT(Headers) FROM features_active ORDER BY ROWID"];
        } else 
        {
            queryString = [NSString stringWithFormat:@"SELECT DISTINCT(Headers) FROM features_micra ORDER BY ROWID"];
        }
            
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(micraDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                tempString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                [headersArray addObject:tempString];
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(micraDB);
        if([headersArray count] > 0)
        {
            [slider scrollToPage:0 animated:NO];
            [slider reloadPages];
        }
    }
}

#pragma Device Orientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setShowdiffBtn:nil];
    [self setHidesimBtn:nil];
    [self setModelPickerView:nil];
    [super viewDidUnload];
}
- (IBAction)showdiffBtnPressed:(UIButton*)sender {
    if (sender.isSelected) {
        sender.selected = FALSE;
        [sender setTitle:@"Show Differences" forState:UIControlStateNormal];
    }
    else
    {
        sender.selected = TRUE;
        [sender setTitle:@"Hide Differences" forState:UIControlStateNormal];
    }
    int previousPageIndex = [slider getCurrentDisplayedPage];
    [slider reloadPages];
    [slider scrollToPage:previousPageIndex animated:NO];
}

- (IBAction)hidesimBtnPressed:(UIButton*)sender {
    if (sender.isSelected) {
        sender.selected = FALSE;
        [sender setTitle:@"Hide Similarities" forState:UIControlStateNormal];
    }
    else
    {
        sender.selected = TRUE;
        [sender setTitle:@"Show Similarities" forState:UIControlStateNormal];
    }
    int previousPageIndex = [slider getCurrentDisplayedPage];
    [slider reloadPages];
    [slider scrollToPage:previousPageIndex animated:NO];
}
@end
