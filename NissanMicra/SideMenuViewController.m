//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import <QuartzCore/QuartzCore.h>
#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "NMFeaturesViewController.h"
#import "NMTechSpecsViewController.h"
#import "NMContactViewController.h"
#import "GalleryGridViewController.h"
#import "NMPriceListViewController.h"
#import "NMTestDriveViewController.h"
#import "NMGalleryViewController.h"
#import "constants.h"

#define HOME @"Home"
#define FEATURES @"Features"
#define TECH_SPEC @"Tech Specs"
#define PRICE_LIST @"Price List"
#define CONTACT_US @"Contact Us"
#define TEST_DRIVE @"Test Drive"
#define GALLERY @"Gallery"


@interface SideMenuViewController()
@end

@implementation SideMenuViewController

@synthesize sideMenu;
@synthesize sideMenuArray;


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    if (self.tableView != nil) {
        UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
        textLabel.textColor = [UIColor whiteColor];
        
        UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:1];
        cellImageView.image = [UIImage imageNamed:@"side_home_act.png"];
        
        NSString *text = [sideMenuArray objectAtIndex:index];
        
        if ([text isEqualToString:HOME]) { //Home
            cellImageView.image = [UIImage imageNamed:@"side_home_act.png"];
        } else if ([text isEqualToString:FEATURES]) { //Features
            cellImageView.image = [UIImage imageNamed:@"side_features_act.png"];
        } else if ([text isEqualToString:TECH_SPEC]) { //Specifications
            cellImageView.image = [UIImage imageNamed:@"side_tech_spec_act.png"];
        } else if ([text isEqualToString:PRICE_LIST]) { //Price List
            cellImageView.image = [UIImage imageNamed:@"side_price_act.png"];
        }else if ([text isEqualToString:TEST_DRIVE]) { //test drive
            cellImageView.image = [UIImage imageNamed:@"side_test_drive.png"];
        }else if ([text isEqualToString:GALLERY]) { //Contact Us
            cellImageView.image = [UIImage imageNamed:@"side_gallery_act.png"];
        }else if ([text isEqualToString:CONTACT_US]) { //Contact Us
            cellImageView.image = [UIImage imageNamed:@"side_contact_act.png"];
        }
    }
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor  = BACKGROUD_COLOR;
    self.tableView.separatorColor   = TABLE_SEPERATOR_COLOR;
    self.tableView.scrollEnabled = YES;
    self.tableView.bounces = NO;
    
    sideMenu.shadowEnabled = TRUE;
    
    sideMenuArray = [[NSMutableArray alloc] init];
    [sideMenuArray addObject:HOME];
    [sideMenuArray addObject:FEATURES];
    [sideMenuArray addObject:TECH_SPEC];
    [sideMenuArray addObject:PRICE_LIST];
    [sideMenuArray addObject:TEST_DRIVE];
    [sideMenuArray addObject:GALLERY];
    [sideMenuArray addObject:CONTACT_US];

    UINavigationBar *tempNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [tempNavigationBar setBarStyle:UIBarStyleDefault];
    tempNavigationBar.tintColor = DEFAULT_COLOR;
    UINavigationItem *navItem = [UINavigationItem alloc];
    navItem.title = @"Menu";
    [tempNavigationBar pushNavigationItem:navItem animated:false];
    
//    tempNavigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
//    tempNavigationBar.layer.shadowOpacity = 1;
//    tempNavigationBar.layer.shadowOffset = CGSizeMake(0,4);
    
    self.tableView.tableHeaderView = tempNavigationBar;
    self.tableView.autoresizesSubviews = TRUE;
   
    index = 0;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:DEFAULTS_GALLERYTOHOME object:nil];
}


#pragma mark -
#pragma mark - UITableViewDataSource

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %d", section];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [sideMenuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UIImageView *cellImageView;
    UILabel *textLabel;
    NSString *text = [sideMenuArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10 , cell.frame.origin.y + 9, 30, 30)];
        cellImageView.contentMode = UIViewContentModeScaleToFill;
        cellImageView.backgroundColor =  [UIColor clearColor];
        cellImageView.tag = 1;
        [cell.contentView addSubview:cellImageView];
        
       
        textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+10, cell.frame.origin.y+12, 150, 30)];
        textLabel.tag = 2;
//        if(indexPath.row == 2)
//            textLabel.font = HELVETICA_FONT(15.0);
//        else
            textLabel.font = HELVETICA_FONT(18.0);
        textLabel.textColor = [UIColor darkGrayColor];
        textLabel.lineBreakMode =  UILineBreakModeTailTruncation;
        textLabel.textAlignment = UITextAlignmentLeft;
        textLabel.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:textLabel];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_SELECTION_COLOR;
        cell.selectedBackgroundView = bgColorView;
        
        cell.textLabel.highlightedTextColor = [UIColor whiteColor];
    }
    
//    cell.textLabel.text = [sideMenuArray objectAtIndex:indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleBlue;
    
    cellImageView = (UIImageView *)[cell viewWithTag:1];
    if ([text isEqualToString:HOME]) { //Home
        cellImageView.image = [UIImage imageNamed:@"side_home.png"];
    } else if ([text isEqualToString:FEATURES]) { //Features
        cellImageView.image = [UIImage imageNamed:@"side_features.png"];
    } else if ([text isEqualToString:TECH_SPEC]) { //Specifications
        cellImageView.image = [UIImage imageNamed:@"side_tech_spec.png"];
    } else if ([text isEqualToString:PRICE_LIST]) { //Price List
        cellImageView.image = [UIImage imageNamed:@"side_price.png"];
    }else if ([text isEqualToString:TEST_DRIVE]) { //test drive
        cellImageView.image = [UIImage imageNamed:@"side_test_drive.png"];
    }else if ([text isEqualToString:GALLERY]) { //Contact Us
        cellImageView.image = [UIImage imageNamed:@"side_gallery.png"];
    }else if ([text isEqualToString:CONTACT_US]) { //Contact Us
        cellImageView.image = [UIImage imageNamed:@"side_contact.png"];
    }
    
    textLabel = (UILabel *)[cell viewWithTag:2];
    textLabel.text = text;
    textLabel.textColor = [UIColor darkGrayColor];
    return cell;
}

#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

#pragma mark -
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
    textLabel.textColor = [UIColor darkGrayColor];
    
    NSString *text = [sideMenuArray objectAtIndex:indexPath.row];
    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:1];
    if ([text isEqualToString:HOME]) { //Home
        cellImageView.image = [UIImage imageNamed:@"side_home.png"];
    } else if ([text isEqualToString:FEATURES]) { //Features
        cellImageView.image = [UIImage imageNamed:@"side_features.png"];
    } else if ([text isEqualToString:TECH_SPEC]) { //Specifications
        cellImageView.image = [UIImage imageNamed:@"side_tech_spec.png"];
    } else if ([text isEqualToString:PRICE_LIST]) { //Price List
        cellImageView.image = [UIImage imageNamed:@"side_price.png"];
    }else if ([text isEqualToString:TEST_DRIVE]) { //test drive
        cellImageView.image = [UIImage imageNamed:@"side_test_drive.png"];
    }else if ([text isEqualToString:GALLERY]) { //Contact Us
        cellImageView.image = [UIImage imageNamed:@"side_gallery.png"];
    } else if ([text isEqualToString:CONTACT_US]) { //Contact Us
        cellImageView.image = [UIImage imageNamed:@"side_contact.png"];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (index == indexPath.row) {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        return;
    }
    
    index = indexPath.row;

    DebugLog(@"row=%d",indexPath.row);
    NSString *text = [sideMenuArray objectAtIndex:indexPath.row];

    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
    textLabel.textColor = [UIColor whiteColor];
    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:1];
    if ([text isEqualToString:HOME]) { //Home
        cellImageView.image = [UIImage imageNamed:@"side_home_act.png"];
    } else if ([text isEqualToString:FEATURES]) { //Features
        cellImageView.image = [UIImage imageNamed:@"side_features_act.png"];
    } else if ([text isEqualToString:TECH_SPEC]) { //Specifications
        cellImageView.image = [UIImage imageNamed:@"side_tech_spec_act.png"];
    } else if ([text isEqualToString:PRICE_LIST]) { //Price List
        cellImageView.image = [UIImage imageNamed:@"side_price_act.png"];
    } else if ([text isEqualToString:TEST_DRIVE]) { //test drive
        cellImageView.image = [UIImage imageNamed:@"side_test_drive_act.png"];
    }else if ([text isEqualToString:GALLERY]) { //Contact Us
        cellImageView.image = [UIImage imageNamed:@"side_gallery_act.png"];
    }else if ([text isEqualToString:CONTACT_US]) { //Contact Us
        cellImageView.image = [UIImage imageNamed:@"side_contact_act.png"];
    }
    
     if ([text isEqualToString:HOME])   //HOME
     {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[GalleryGridViewController class]])
        {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        return;
        }
        GalleryGridViewController *homeController = [[GalleryGridViewController alloc] initWithNibName:@"GalleryGridViewController" bundle:nil];
        homeController.title = @"NISSAN MICRA";
        NSArray *controllers = [NSArray arrayWithObject:homeController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
         controllers = nil;
     }
    else if ([text isEqualToString:FEATURES])  //Features
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[NMFeaturesViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        NMFeaturesViewController *contactController = [[NMFeaturesViewController alloc] initWithNibName:@"NMFeaturesViewController" bundle:nil];
        contactController.title = FEATURES;
        NSArray *controllers = [NSArray arrayWithObject:contactController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    else if ([text isEqualToString:TECH_SPEC])  //Technical Specifications
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[NMTechSpecsViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        NMTechSpecsViewController *contactController = [[NMTechSpecsViewController alloc] initWithNibName:@"NMTechSpecsViewController" bundle:nil];
        contactController.title = TECH_SPEC;
        NSArray *controllers = [NSArray arrayWithObject:contactController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    else if ([text isEqualToString:PRICE_LIST])  //Price List
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[NMPriceListViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        NMPriceListViewController *contactController = [[NMPriceListViewController alloc] initWithNibName:@"NMPriceListViewController" bundle:nil];
        contactController.title = PRICE_LIST;
        NSArray *controllers = [NSArray arrayWithObject:contactController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    else if ([text isEqualToString:TEST_DRIVE])  //TEST_DRIVE
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[NMTestDriveViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        NMTestDriveViewController *testDriveController = [[NMTestDriveViewController alloc] initWithNibName:@"NMTestDriveViewController" bundle:nil];
        testDriveController.title = TEST_DRIVE;
        NSArray *controllers = [NSArray arrayWithObject:testDriveController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    else if ([text isEqualToString:GALLERY])  //TEST_DRIVE
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[NMGalleryViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        NMGalleryViewController *galleryController = [[NMGalleryViewController alloc] initWithNibName:@"NMGalleryViewController" bundle:nil];
        galleryController.title = GALLERY;
        NSArray *controllers = [NSArray arrayWithObject:galleryController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:DEFAULTS_GALLERY];
        [userDefaults synchronize];
    }
    else if ([text isEqualToString:CONTACT_US])  //Contact Us
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[NMContactViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        NMContactViewController *contactController = [[NMContactViewController alloc] initWithNibName:@"NMContactViewController" bundle:nil];
        contactController.title = CONTACT_US;
        NSArray *controllers = [NSArray arrayWithObject:contactController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
}

-(void)handleNotification:(NSNotification *)notification
{
     NSDictionary *dict = [notification userInfo];
    BOOL openHomeTab = [[dict objectForKey:DEFAULTS_GALLERY] boolValue];
    DebugLog(@"openHome Tab %d",openHomeTab);
    if (openHomeTab)
    {
        UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
        textLabel.textColor = [UIColor darkGrayColor];
        UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:1];
        cellImageView.image = [UIImage imageNamed:@"side_gallery.png"];
        cell = nil;
        textLabel = nil;
        cellImageView = nil;
        
        index = 0;
        cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        textLabel = (UILabel *)[cell viewWithTag:2];
        textLabel.textColor = [UIColor whiteColor];
        cellImageView = (UIImageView *)[cell viewWithTag:1];
        cellImageView.image = [UIImage imageNamed:@"side_home_act.png"];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        
        DebugLog(@"self.sideMenu.navigationController.viewControllers %@",self.sideMenu.navigationController.viewControllers);
        
        GalleryGridViewController *homeController = [[GalleryGridViewController alloc] initWithNibName:@"GalleryGridViewController" bundle:nil];
        homeController.title = @"NISSAN MICRA";
        NSArray *controllers = [NSArray arrayWithObject:homeController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;

        DebugLog(@"self.sideMenu.navigationController.viewControllers %@",self.sideMenu.navigationController.viewControllers);
        
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
