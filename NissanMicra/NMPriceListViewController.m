//
//  NMPriceListViewController.m
//  NissanMicra
//
//  Created by Rishi on 21/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "NMPriceListViewController.h"
#import "MFSideMenu.h"
#import "NMAppDelegate.h"
#import "MBProgressHUD.h"
#import "NMPriceListObject.h"


#define DEFAULT_STATE @"Delhi"
#define PRICE_LINK @"http://stage.phonethics.in/proj/neon/micra_newpricelist.php"
//#define PRICE_LINK @"http://192.168.254.203:81/micra_newpricelist.txt"

/*
 select state from price_micra group by state
 select city from price_micra where state='West Bengal'
 SELECT state FROM price_micra ORDER BY state ASC LIMIT 1
 */
@interface NMPriceListViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation NMPriceListViewController
@synthesize progressHUD;
@synthesize pricelistArray;
@synthesize statePicker;
@synthesize cityPicker;
@synthesize modelPicker;
@synthesize selectedState;
@synthesize selectedCity;
@synthesize stateArray;
@synthesize cityArray;
@synthesize modelArray;
@synthesize stateBtn;
@synthesize cityBtn;
@synthesize pricelistTableView;
@synthesize selectedCategory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (priceconnection != nil) {
        [priceconnection cancel];
        priceconnection = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.pricelistTableView.backgroundColor = BACKGROUD_COLOR;

    [self changeButtonFontAndTextColor:stateBtn];
    [self changeButtonFontAndTextColor:cityBtn];
    [stateBtn setTitle:@"State" forState:UIControlStateNormal];
    [stateBtn setTitle:@"State" forState:UIControlStateHighlighted];
    [cityBtn setTitle:@"Cities" forState:UIControlStateNormal];
    [cityBtn setTitle:@"Cities" forState:UIControlStateHighlighted];
    [self setupMenuBarButtonItems];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithTitle:@"MODEL" style:UIBarButtonSystemItemDone target:self action:@selector(model_Clicked:)];

    pricelistArray = [[NSMutableArray alloc] init];
    stateArray = [[NSMutableArray alloc] init];
    cityArray = [[NSMutableArray alloc] init];
    modelArray = [[NSMutableArray alloc] init];
    [modelArray addObject:PETROL];
    [modelArray addObject:DIESEL];
    [modelArray addObject:ACTIVE];
    
    selectedState = @"";
    selectedCity = @"";
    
    [self addPickerWithDoneButton];
    selectedCategory = [modelArray objectAtIndex:0];
    self.navigationItem.leftBarButtonItem.title = selectedCategory;
    DebugLog(@"[NM_APP_DELEGATE networkavailable] %d",[NM_APP_DELEGATE networkavailable]);
    if ([NM_APP_DELEGATE networkavailable]) {
         [self sendHttpRequest:PRICE_LINK];
    }
    else{
        [self loadStateArray];
         if (stateArray.count <= 0) {
            UIAlertView *errorView = [[UIAlertView alloc]
                                       initWithTitle:@"No Network Connection"
                                       message:@"Please check your internet connection and try again."
                                       delegate:self
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
            [errorView show];
        }
        else{
            [self loadObjectArray];
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:selectedCategory,@"Model",selectedState,@"State",selectedCity,@"City",nil];
            [Flurry logEvent:@"PriceList_Tab_Event" withParameters:params];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NetworkNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
}
-(void)NetworkNotifyCallBack
{
    DebugLog(@"Price List : Got Network Notification");
    [self performSelector:@selector(sendHttpRequest:) withObject:PRICE_LINK afterDelay:2.0];
}

-(void)changeButtonFontAndTextColor:(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:TABLE_SELECTION_COLOR forState:UIControlStateHighlighted];
    [btn setBackgroundColor:[UIColor grayColor]];
    btn.titleLabel.font = HELVETICA_FONT(14);
    btn.titleLabel.minimumFontSize = 10;
    btn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
    btn.layer.borderWidth = 2.0;
    btn.titleLabel.numberOfLines = 2;
    btn.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Bar Button method
- (void) model_Clicked:(id)sender {
    [statePicker setHidden:TRUE];
    [modelPicker setHidden:FALSE];
    [cityPicker setHidden:TRUE];
    UISegmentedControl *doneButton = (UISegmentedControl *)[actionSheet viewWithTag:1000];
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    modelPicker.frame = CGRectMake(modelPicker.frame.origin.x,modelPicker.frame.origin.y,self.view.frame.size.width,216);
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, self.view.frame.size.width,485)];
}

#pragma Model Picker method
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    
    statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    statePicker.showsSelectionIndicator = YES;
    statePicker.dataSource = self;
    statePicker.delegate = self;
    [actionSheet addSubview:statePicker];
    
    
    cityPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    cityPicker.showsSelectionIndicator = YES;
    cityPicker.dataSource = self;
    cityPicker.delegate = self;
    [actionSheet addSubview:cityPicker];
    
    modelPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    modelPicker.showsSelectionIndicator = YES;
    modelPicker.dataSource = self;
    modelPicker.delegate = self;
    [actionSheet addSubview:modelPicker];
    
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    doneButton.tag = 1000;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    [statePicker reloadAllComponents];
    [cityPicker reloadAllComponents];
    [modelPicker reloadAllComponents];
    
    if([statePicker numberOfRowsInComponent:0] != -1 && stateArray.count > 0)
    {
        [statePicker selectRow:0 inComponent:0 animated:YES];
    }
    if([cityPicker numberOfRowsInComponent:0] != -1 && cityArray.count > 0)
    {
        [cityPicker selectRow:0 inComponent:0 animated:YES];
    }
    if([modelPicker numberOfRowsInComponent:0] != -1 && modelArray.count > 0)
    {
        [modelPicker selectRow:0 inComponent:0 animated:YES];
    }
}

#pragma mark Picker Button method
- (void)doneFilterBtnClicked:(id)sender
{
    if(!statePicker.isHidden)
    {
        if (stateArray.count > 0) {
            selectedState = [stateArray objectAtIndex:[statePicker selectedRowInComponent:0]];
            [stateBtn setTitle:selectedState forState:UIControlStateNormal];
            [stateBtn setTitle:selectedState forState:UIControlStateHighlighted];
            [self loadCityArray];
        }
    } else if(!cityPicker.isHidden) {
        if (cityArray.count > 0) {
            selectedCity = [cityArray objectAtIndex:[cityPicker selectedRowInComponent:0]];
            [cityBtn setTitle:selectedCity forState:UIControlStateNormal];
            [cityBtn setTitle:selectedCity forState:UIControlStateHighlighted];
        }
    } else if(!modelPicker.isHidden) {
        if (modelArray.count > 0) {
            selectedCategory = [modelArray objectAtIndex:[modelPicker selectedRowInComponent:0]];
            self.navigationItem.leftBarButtonItem.title = selectedCategory;
        }
    }
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    [self loadObjectArray];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:selectedCategory,@"Model",selectedState,@"State",selectedCity,@"City",nil];
    [Flurry logEvent:@"PriceList_Tab_Event" withParameters:params];
}

- (void)cancelFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark NSURLConnection Call Method
-(void)sendHttpRequest:(NSString *)urlString
{
    [self showProgressHUDWithMessage:@"Loading"];
    DebugLog(@"URL : %@",urlString);
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:YES timeoutInterval:10.0];
	priceconnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"didFailWithError: %@", [error localizedDescription]);
    [self hideProgressHUD:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self hideProgressHUD:YES];
    NSError* error;
	if(connection==priceconnection)
	{
        if(responseAsyncData != nil)
        {
            NSArray* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
//            NSLog(@"json: %@", json);
            
//            NSArray *json = @[@{@"ID": @"13",@"State": @"Andhra Pradesh",@"City": @"Hyderabad",@"PET_XL": @"488,432",@"PET_XL_O": @"552,552", @"PET_XV_CVT": @"650,614",@"DL_XE": @"50",@"DL_XL": @"610,566",@"DL_XL_O": @"654,330",@"DL_XV": @"701,713",@"DL_XV_P": @"726,694",@"ACT_XE": @"356,222",@"ACT_XL": @"408,841",@"ACT_XV": @"447,701",@"ACT_XV_S": @"479,436"},@{@"ID": @"13",@"State": @"Assam",@"City": @"Tinsukia",@"PET_XL": @"488,432",@"PET_XL_O": @"552,552", @"PET_XV_CVT": @"650,614",@"DL_XL": @"610,566",@"DL_XL_O": @"654,330",@"DL_XV": @"701,713",@"DL_XV_P": @"726,694",@"ACT_XE": @"356,222",@"ACT_XL": @"408,841",@"ACT_XV": @"447,701",@"ACT_XV_S": @"479,436"}];
            DebugLog(@"json: %@", json);
            if(json != nil) {
                [pricelistArray removeAllObjects];
                for (NSDictionary* songattributeDict in json)
                {
                    priceObj = [[NMPriceListObject alloc] init];
                    priceObj.priceId = [songattributeDict objectForKey:@"ID"];
                    priceObj.state = [songattributeDict objectForKey:@"State"];
                    priceObj.city = [songattributeDict objectForKey:@"City"];
                    priceObj.PET_XL = [songattributeDict objectForKey:@"PET_XL"];
                    priceObj.PET_XL_O = [songattributeDict objectForKey:@"PET_XL_O"];
                    priceObj.PET_XV_CVT = [songattributeDict objectForKey:@"PET_XV_CVT"];
                    if([songattributeDict valueForKey:@"DL_XE"] == nil)
                    {
                        priceObj.DL_XE = @"";
                    }else{
                        priceObj.DL_XE = [songattributeDict objectForKey:@"DL_XE"];
                    }
                    priceObj.DL_XL = [songattributeDict objectForKey:@"DL_XL"];
                    priceObj.DL_XL_O = [songattributeDict objectForKey:@"DL_XL_O"];
                    priceObj.DL_XV = [songattributeDict objectForKey:@"DL_XV"];
                    priceObj.DL_XV_P = [songattributeDict objectForKey:@"DL_XV_P"];
                    priceObj.ACT_XE = [songattributeDict objectForKey:@"ACT_XE"];
                    priceObj.ACT_XL = [songattributeDict objectForKey:@"ACT_XL"];
                    priceObj.ACT_XV = [songattributeDict objectForKey:@"ACT_XV"];
                    priceObj.ACT_XV_S = [songattributeDict objectForKey:@"ACT_XV_S"];

                    [pricelistArray addObject:priceObj];
                    priceObj = nil;
                }
            }
        }
        responseAsyncData = nil;
        priceconnection = nil;
        [self deletePriceListTable];
        [self addPriceListTable];
        [self loadStateArray];
        [self loadObjectArray];
        
        if(stateArray.count > 0){
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:selectedCategory,@"Model",selectedState,@"State",selectedCity,@"City",nil];
            [Flurry logEvent:@"PriceList_Tab_Event" withParameters:params];
        }
	}
}


#pragma mark HUD methods
- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}

-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}

- (void)addPriceListTable
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[NM_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &micraDB) == SQLITE_OK)
    {
        for (int index=0; index<[pricelistArray count] ; index++)
        {
            NMPriceListObject *temoObj = [pricelistArray objectAtIndex:index];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT INTO price_micra (price_id, state, city, PET_XL, PET_XL_O, PET_XV_CVT,DL_XE, DL_XL, DL_XL_O, DL_XV, DL_XV_P, ACT_XE, ACT_XL, ACT_XV, ACT_XV_S) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",temoObj.priceId,temoObj.state,temoObj.city,temoObj.PET_XL,temoObj.PET_XL_O,temoObj.PET_XV_CVT,temoObj.DL_XE,temoObj.DL_XL,temoObj.DL_XL_O,temoObj.DL_XV,temoObj.DL_XV_P,temoObj.ACT_XE,temoObj.ACT_XL,temoObj.ACT_XV,temoObj.ACT_XV_S];
            DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(micraDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(micraDB);
                DebugLog(@"inserted id========%d", lastrowid);
            } else {
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(micraDB);
    [pricelistArray removeAllObjects];
}

-(void)deletePriceListTable
{
    sqlite3_stmt    *statement;
    
     const char *dbpath = [[NM_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &micraDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM price_micra"];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(micraDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted");
        } else {
            DebugLog(@"Deletion failed.");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(micraDB);
}

-(void)loadObjectArray
{
    DebugLog(@"====loadObjectArray===");
    DebugLog(@"selectedCategory %@ state %@ city %@",selectedCategory,selectedState,selectedCity);

    if (sqlite3_open([[NM_APP_DELEGATE databasePath] UTF8String], &micraDB) == SQLITE_OK) {
        NSString *queryString;
        if([selectedCategory isEqualToString:PETROL])
        {
            queryString = [NSString stringWithFormat:@"select PET_XL,PET_XL_O,PET_XV_CVT from price_micra where state =  \"%@\" and city = \"%@\"", selectedState, selectedCity];
        } else if([selectedCategory isEqualToString:DIESEL]) {
            queryString = [NSString stringWithFormat:@"select DL_XE,DL_XL,DL_XL_O,DL_XV,DL_XV_P from price_micra where state =  \"%@\" and city = \"%@\"", selectedState, selectedCity];
        } else {
            queryString = [NSString stringWithFormat:@"select ACT_XE,ACT_XL,ACT_XV,ACT_XV_S from price_micra where state =  \"%@\" and city = \"%@\"", selectedState, selectedCity];
        }
        DebugLog(@"%@",queryString);
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(micraDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            [pricelistArray removeAllObjects];
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                if([selectedCategory isEqualToString:PETROL])
                {
                    NSString *string1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *string2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *string3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSDictionary *petrolDict1 = [[NSDictionary alloc] initWithObjectsAndKeys:string1,@"XL", nil];
                    NSDictionary *petrolDict2 = [[NSDictionary alloc] initWithObjectsAndKeys:string2,@"XL(O)", nil];
                    NSDictionary *petrolDict3 = [[NSDictionary alloc] initWithObjectsAndKeys:string3,@"XV CVT", nil];
                    [pricelistArray addObject:petrolDict1];
                    [pricelistArray addObject:petrolDict2];
                    [pricelistArray addObject:petrolDict3];
                } else if([selectedCategory isEqualToString:DIESEL]) {
                    NSString *string1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *string2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *string3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *string4 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    NSString *string5 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 4)];
                    if (string1 == nil || [string1 isEqualToString:@""]) {
                        DebugLog(@"Not found");
                    }else{
                        NSDictionary *petrolDict1 = [[NSDictionary alloc] initWithObjectsAndKeys:string1,@"XE", nil];
                        [pricelistArray addObject:petrolDict1];
                    }
                    NSDictionary *petrolDict2 = [[NSDictionary alloc] initWithObjectsAndKeys:string2,@"XL", nil];
                    NSDictionary *petrolDict3 = [[NSDictionary alloc] initWithObjectsAndKeys:string3,@"XL(O)", nil];
                    NSDictionary *petrolDict4 = [[NSDictionary alloc] initWithObjectsAndKeys:string4,@"XV", nil];
                    NSDictionary *petrolDict5 = [[NSDictionary alloc] initWithObjectsAndKeys:string5,@"XV(P)", nil];
                    [pricelistArray addObject:petrolDict2];
                    [pricelistArray addObject:petrolDict3];
                    [pricelistArray addObject:petrolDict4];
                    [pricelistArray addObject:petrolDict5];
                } else {
                    NSString *string1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    NSString *string2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                    NSString *string3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
                    NSString *string4 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                    NSDictionary *petrolDict1 = [[NSDictionary alloc] initWithObjectsAndKeys:string1,@"XE", nil];
                    NSDictionary *petrolDict2 = [[NSDictionary alloc] initWithObjectsAndKeys:string2,@"XL", nil];
                    NSDictionary *petrolDict3 = [[NSDictionary alloc] initWithObjectsAndKeys:string3,@"XV", nil];
                    NSDictionary *petrolDict4 = [[NSDictionary alloc] initWithObjectsAndKeys:string4,@"XV(S)", nil];
                    [pricelistArray addObject:petrolDict1];
                    [pricelistArray addObject:petrolDict2];
                    [pricelistArray addObject:petrolDict3];
                    [pricelistArray addObject:petrolDict4];
                }
            }
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(micraDB);
    DebugLog(@"priceListArray %@",pricelistArray);
    [pricelistTableView reloadData];
}


-(void)loadStateArray
{
    DebugLog(@"====loadStateArray===");
    if (sqlite3_open([[NM_APP_DELEGATE databasePath] UTF8String], &micraDB) == SQLITE_OK) {
        NSString *statequeryString = [NSString stringWithFormat:@"select state from price_micra group by state"];
        DebugLog(@"%@",statequeryString);
        const char *statequeryCharP = [statequeryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(micraDB, statequeryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            [stateArray removeAllObjects];
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                NSString *stateString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                [stateArray addObject:stateString];
            }
        }
        sqlite3_finalize(stmt);
        
    }
    sqlite3_close(micraDB);
    if (stateArray.count > 0) {
        selectedState = DEFAULT_STATE;//[stateArray objectAtIndex:0];
        [stateBtn setTitle:selectedState forState:UIControlStateNormal];
        [stateBtn setTitle:selectedState forState:UIControlStateHighlighted];
        [self loadCityArray];
        [statePicker reloadAllComponents];
        DebugLog(@"selectedState %d",[stateArray indexOfObject:selectedState]);
        [statePicker selectRow:[stateArray indexOfObject:selectedState] inComponent:0 animated:YES];
    }
}

-(void)loadCityArray
{
    DebugLog(@"====loadCityArray===");
    if (sqlite3_open([[NM_APP_DELEGATE databasePath] UTF8String], &micraDB) == SQLITE_OK) {
        NSString *cityqueryString = [NSString stringWithFormat:@"select city from price_micra where state = \"%@\"" , selectedState];
        DebugLog(@"%@",cityqueryString);
        const char *cityqueryCharP = [cityqueryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(micraDB, cityqueryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            [cityArray removeAllObjects];
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                NSString *cityString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                [cityArray addObject:cityString];
            }
        }
        sqlite3_finalize(stmt);
        
    }
    sqlite3_close(micraDB);
    if (cityArray.count > 0) {
        selectedCity = [cityArray objectAtIndex:0];
        [cityBtn setTitle:selectedCity forState:UIControlStateNormal];
        [cityBtn setTitle:selectedCity forState:UIControlStateHighlighted];
        [cityPicker reloadAllComponents];
        [cityPicker selectRow:0 inComponent:0 animated:YES];
    }
}


#pragma mark Pickerview methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    if(thePickerView==statePicker)
    {
        return [stateArray count];
    } else if(thePickerView==cityPicker){
        return [cityArray count];
    } else {
        return [modelArray count];
    }
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(thePickerView==statePicker)
    {
        return [stateArray objectAtIndex:row];
    } else if(thePickerView==cityPicker){
        return [cityArray objectAtIndex:row];
    } else {
        return [modelArray objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(thePickerView==statePicker)
    {
        DebugLog(@"%@",[stateArray objectAtIndex:row]);
    } else if(thePickerView==cityPicker) {
        DebugLog(@"%@",[cityArray objectAtIndex:row]);
    } else  {
        DebugLog(@"%@",[modelArray objectAtIndex:row]);
    }
}


#pragma mark Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,45)];
    customView.backgroundColor = [UIColor whiteColor];
    UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel1.backgroundColor = [UIColor clearColor];
    headerLabel1.frame = CGRectMake(0, 0, 160, 45);
    headerLabel1.font = HELVETICA_FONT(12);
    headerLabel1.text = @"Car Model";
    headerLabel1.textAlignment = UITextAlignmentCenter;
    headerLabel1.layer.borderWidth = 1.0;
    headerLabel1.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel1.textColor = [UIColor blackColor];
    headerLabel1.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [customView addSubview:headerLabel1];
    
    UILabel *headerLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel2.backgroundColor = [UIColor clearColor];
    headerLabel2.frame = CGRectMake(CGRectGetMaxX(headerLabel1.frame), 0, 160, 45);
    headerLabel2.font = HELVETICA_FONT(12);
    headerLabel2.text = @"Price";
    headerLabel2.textAlignment = UITextAlignmentCenter;
    headerLabel2.layer.borderWidth = 1.0;
    headerLabel2.layer.borderColor = [UIColor blackColor].CGColor;
    headerLabel2.textColor = [UIColor blackColor];
    headerLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [customView addSubview:headerLabel2];
    
    return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 90;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,90)];
    customView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel1.backgroundColor = [UIColor clearColor];
    headerLabel1.frame = CGRectMake(10, 0, 310, 50);
    headerLabel1.font = HELVETICA_FONT(12);
    headerLabel1.text = @"* Accessories may not be a part of standard fitment, please check with your dealer for details";
    headerLabel1.textAlignment = UITextAlignmentLeft;
    headerLabel1.numberOfLines = 0;
    headerLabel1.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel1.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel1];
    
    
    UILabel *headerLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel2.backgroundColor = [UIColor clearColor];
    headerLabel2.frame = CGRectMake(10,CGRectGetMaxY(headerLabel1.frame), 310,40);
    headerLabel2.font = HELVETICA_FONT(12);
    if (selectedCity == nil || [selectedCity isEqualToString:@""]) {
        headerLabel2.text = @"";
    }else{
        headerLabel2.text = [NSString stringWithFormat:@"** All prices shown are ex-showroom %@",selectedCity];
    }
    headerLabel2.textAlignment = UITextAlignmentLeft;
    headerLabel2.numberOfLines = 0;
    headerLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    headerLabel1.textColor = [UIColor blackColor];
    [customView addSubview:headerLabel2];
    
    return customView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [pricelistArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //DebugLog(@"W==%f H==%f",self.view.frame.size.width,self.view.frame.size.height);
    UILabel *cellLabel1;
    UILabel *cellLabel2;
    
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        cellLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
        cellLabel1.backgroundColor = [UIColor clearColor];
        cellLabel1.frame = CGRectMake(0, 0, 160, 45);
        cellLabel1.font = HELVETICA_FONT(12);
        cellLabel1.tag = 101;
        cellLabel1.numberOfLines = 2;
        cellLabel1.textAlignment = UITextAlignmentCenter;
        cellLabel1.textColor = [UIColor blackColor];
        cellLabel1.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [cell.contentView addSubview:cellLabel1];
        
        cellLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
        cellLabel2.backgroundColor = [UIColor clearColor];
        cellLabel2.frame = CGRectMake(CGRectGetMaxX(cellLabel1.frame), 0, 160, 45);
        cellLabel2.font = HELVETICA_FONT(12);
        cellLabel2.tag = 102;
        cellLabel2.numberOfLines = 2;
        cellLabel2.textAlignment = UITextAlignmentCenter;
        cellLabel2.textColor = [UIColor blackColor];
        cellLabel2.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [cell.contentView addSubview:cellLabel2];

        
    }
    cellLabel1 = (UILabel *)[cell viewWithTag:101];
    cellLabel2 = (UILabel *)[cell viewWithTag:102];
    
    NSDictionary *tempDict = (NSDictionary *)[pricelistArray objectAtIndex:indexPath.row];
    cellLabel1.text = [tempDict.allKeys objectAtIndex:0];
    cellLabel2.text = [tempDict valueForKey:[tempDict.allKeys objectAtIndex:0]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setStateBtn:nil];
    [self setCityBtn:nil];
    [self setPricelistTableView:nil];
    [self setStatePicker:nil];
    [self setCityPicker:nil];
    [self setModelPicker:nil];
    [super viewDidUnload];
}

- (IBAction)stateBtnPressed:(id)sender {
    
    if (stateArray.count > 0) {
        [cityPicker setHidden:TRUE];
        [modelPicker setHidden:TRUE];
        [statePicker setHidden:FALSE];
        UISegmentedControl *doneButton = (UISegmentedControl *)[actionSheet viewWithTag:1000];
        doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
        statePicker.frame = CGRectMake(statePicker.frame.origin.x,statePicker.frame.origin.y,self.view.frame.size.width,216);
        [actionSheet showInView:self.view];
        [actionSheet setBounds:CGRectMake(0, 0, self.view.frame.size.width,485)];
    }
}

- (IBAction)cityBtnPressed:(id)sender {
    if (cityArray.count > 0) {
        [statePicker setHidden:TRUE];
        [modelPicker setHidden:TRUE];
        [cityPicker setHidden:FALSE];
        UISegmentedControl *doneButton = (UISegmentedControl *)[actionSheet viewWithTag:1000];
        doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
        cityPicker.frame = CGRectMake(cityPicker.frame.origin.x,cityPicker.frame.origin.y,self.view.frame.size.width,216);
        [actionSheet showInView:self.view];
        [actionSheet setBounds:CGRectMake(0, 0, self.view.frame.size.width,485)];
    }
}

#pragma Device Orientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
