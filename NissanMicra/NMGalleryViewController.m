//
//  NMGalleryViewController.m
//  NissanMicra
//
//  Created by Kirti Nikam on 10/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMGalleryViewController.h"
#import "MBProgressHUD.h"
#import "NMAppDelegate.h"
#import "MFSideMenu.h"
#import "CarUtilGallery.h"
#import "GalleryGridViewController.h"

#define MICRA_GALLERY_LINK @"http://stage.phonethics.in/proj/neon/micra_gallery.php"

@interface NMGalleryViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation NMGalleryViewController
@synthesize galleryArray;
@synthesize photos = _photos;
@synthesize socialLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (conn != nil) {
        [conn cancel];
        conn = nil;
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([[userDefaults objectForKey:DEFAULTS_GALLERY] boolValue] == YES) {
        [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:DEFAULTS_GALLERY];
        [userDefaults synchronize];
    }else{
        //send open Home tab notification to sidemenu
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        self.title = NSLocalizedString(@"NISSAN MICRA", @"NISSAN MICRA");
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:DEFAULTS_GALLERY];
        [[NSNotificationCenter defaultCenter] postNotificationName:DEFAULTS_GALLERYTOHOME object:nil userInfo:dictionary];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupMenuBarButtonItems];
    [Flurry logEvent:@"Gallery_Tab_Event"];
//    if([NM_APP_DELEGATE networkavailable])
//    {
//        DebugLog(@"networkavailable");
//        [self carModelsListAsynchronousCall];
//    }
//    else  {
        DebugLog(@"network not available");
        [self parseFromFile];
//    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NetworkNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
}
-(void)NetworkNotifyCallBack
{
    DebugLog(@"Home : Got Network Notification");
    [self performSelector:@selector(carModelsListAsynchronousCall) withObject:nil afterDelay:2.0];
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu_icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}
-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
-(void)carModelsListAsynchronousCall
{
	/****************Asynchronous Request**********************/
    [self showProgressHUDWithMessage:@"Loading"];
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:MICRA_GALLERY_LINK] cachePolicy:YES timeoutInterval:20.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
    conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    [self hideProgressHUD:YES];
    DebugLog(@"didFailWithError Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
        DebugLog(@"\n result:%@\n\n", result);
        [NM_APP_DELEGATE writeToTextFile:result name:@"gallery"];
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
		responseAsyncData = nil;
        conn = nil;
        result = nil;
	}
    else {
        [self parseFromFile];
    }
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [NM_APP_DELEGATE getTextFromFile:@"gallery"];
    //DebugLog(@"\n data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
        [xmlParser setDelegate:self];
        [xmlParser parse];
    }
    else
    {
        [self hideProgressHUD:YES];
        [self performSelector:@selector(showAlert) withObject:nil afterDelay:1.0];
    }
}
-(void)showAlert
{
    UIAlertView *errorView = [[UIAlertView alloc]
                              initWithTitle:ALERT_TITLE
                              message:@"Please check your internet connection and try again."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [errorView show];
}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"micragallery"])
	{
        if (galleryArray == nil) {
            galleryArray = [[NSMutableArray alloc] init];
        }
        else
        {
            [galleryArray removeAllObjects];
        }
        socialLink = [attributeDict objectForKey:@"sociallink"];

	} else if([elementName isEqualToString:@"photo"])
    {
        if ([[[attributeDict objectForKey:@"group"] lowercaseString] hasPrefix:@"gallery"]) {
            DebugLog(@"got gallery");
            galleryObj = [[CarUtilGallery alloc] init];
            galleryObj.ilink = [attributeDict objectForKey:@"ilink"];
            galleryObj.vlink = [attributeDict objectForKey:@"vlink"];
            galleryObj.caption = [attributeDict objectForKey:@"caption"];
            galleryObj.slink = [NSString stringWithFormat:@"%@%@/",socialLink,[attributeDict objectForKey:@"name"]];
            galleryObj.thumbnail = [attributeDict objectForKey:@"thumbnail"];
            galleryObj.group = [attributeDict objectForKey:@"group"];
            galleryObj.description = [attributeDict objectForKey:@"description"];
        }
    }
//    else if([elementName isEqualToString:@"description"]) {
//        elementFound = YES;
//    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
//    if (elementFound)
//    {
//        galleryObj.description = string;
//    }
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"micragallery"]) {
        DebugLog(@"galleryArray %@",galleryArray);
        [self hideProgressHUD:YES];
        if (galleryArray.count > 0) {
            [self pushGalleryController];
        }
    }
    else if([elementName isEqualToString:@"photo"]) {
        elementFound = NO;
        DebugLog(@" -%@- -%@-", galleryObj.group,galleryObj.thumbnail);
        if(galleryObj.description == nil)
        {
            galleryObj.description = @"";
        }
        if ([[galleryObj.group lowercaseString] hasPrefix:@"gallery"]) {
            [galleryArray addObject:galleryObj];
            galleryObj = nil;
        }
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}

-(void)pushGalleryController {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([[userDefaults objectForKey:DEFAULTS_GALLERY] boolValue] == YES) {
        [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:DEFAULTS_GALLERY];
        [userDefaults synchronize];
    }
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    
    for(int i = 0; i < [galleryArray count]; i++)
    {
        CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[galleryArray objectAtIndex:i];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:tempgalleryObj.ilink]];
        photo.slink = tempgalleryObj.slink;
        photo.vlink = tempgalleryObj.vlink;
        photo.caption = [NSString stringWithFormat:@"%@\n%@",tempgalleryObj.caption,tempgalleryObj.description];
        [photos addObject:photo];
    }
    self.photos = photos;
    
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.wantsFullScreenLayout = YES;
    [browser setInitialPageIndex:0];
    [self.navigationController pushViewController:browser animated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)lindex {
    if (lindex < _photos.count)
        return [_photos objectAtIndex:lindex];
    return nil;
}

- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser slinkAtIndex:(int)lindex {
    if (lindex < _photos.count)
    {
        MWPhoto *tempPhotoObj = (MWPhoto *)[_photos objectAtIndex:lindex];
        return tempPhotoObj.slink;
    }
    return nil;
}

- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser vlinkAtIndex:(int)lindex {
    if (lindex < _photos.count)
    {
        MWPhoto *tempPhotoObj = (MWPhoto *)[_photos objectAtIndex:lindex];
        return tempPhotoObj.vlink;
    }
    return @"";
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
