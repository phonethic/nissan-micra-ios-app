//
//  NMDieselModelObject.h
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMDieselModelObject : NSObject
@property (nonatomic, copy) NSString *Feature;
@property (nonatomic, copy) NSString *XE;
@property (nonatomic, copy) NSString *XL;
@property (nonatomic, copy) NSString *XL_O;
@property (nonatomic, copy) NSString *XV;
@property (nonatomic, copy) NSString *XV_Pre;
@property (nonatomic, readwrite) BOOL is_same;
@end
