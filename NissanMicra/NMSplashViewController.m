//
//  NMSplashViewController.m
//  NissanMicra
//
//  Created by Rishi on 24/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMSplashViewController.h"
#import "TTUIScrollViewSlidingPages.h"
#import "TTSlidingPage.h"
#import "TTSlidingPageTitle.h"
#import "NMSplashImageViewController.h"
#import "NMAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"

static const CGFloat gradientWidth = 0.2;
static const CGFloat gradientDimAlpha = 0.5;
static const int animationFramesPerSec = 8;

@interface NMSplashViewController ()

@end

@implementation NMSplashViewController
@synthesize goBtn;
@synthesize imagelinkArray;
@synthesize logoimageView;
@synthesize glowBtn;
@synthesize imageView1,imageView2;
@synthesize autoAnimationTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (stopAnimation == NO) {
        stopAnimation = YES;
    }
    if (conn != nil) {
        [conn cancel];
        conn = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //imagelinkArray = [[NSMutableArray alloc] init];
    if([NM_APP_DELEGATE networkavailable])
    {
         [self sendHttpRequest:SPLASH_LINK];
    }
    else  {
        [self parseFromFile];
    }
   
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    slider = [[TTScrollSlidingPagesController alloc] init];
    slider.disableUIPageControl = YES;
    slider.titleScrollerHidden = YES;
    slider.initialPageNumber = 0;
    slider.minimumZoom = 0.0;
    slider.dataSource = self; /*the current view controller (self) conforms to the TTSlidingPagesDataSource protocol)*/
    slider.view.frame = self.view.frame; //I'm setting up the view to be fullscreen in the current view
    [self.view addSubview:slider.view];
    [self addChildViewController:slider];
    
    glowBtn.titleLabel.layer.delegate = self;
    glowBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    glowBtn.titleLabel.font = HELVETICA_FONT(30);
    glowBtn.titleLabel.textColor = [UIColor whiteColor];
    glowBtn.titleLabel.alpha = 1.0;
    [glowBtn setTitle:@"Enter" forState:UIControlStateNormal];
    [glowBtn setTitle:@"Enter" forState:UIControlStateDisabled];
    [glowBtn setBackgroundColor:[UIColor blackColor]];
    
    
//    UITapGestureRecognizer *imageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(imageViewTapDetected:)];
//    imageViewTap.numberOfTapsRequired = 1;
//    imageViewTap.cancelsTouchesInView = NO;
//    [self.imageView1  addGestureRecognizer:imageViewTap];
//    [self.imageView2  addGestureRecognizer:imageViewTap];
    

 //   [glowBtn setAlpha:0.7];
    [self startTimer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NetworkNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
}
-(void)NetworkNotifyCallBack
{
    DebugLog(@"Splash : Got Network Notification");    
    [self performSelector:@selector(sendHttpRequest:) withObject:SPLASH_LINK afterDelay:2.0];
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [NM_APP_DELEGATE getTextFromFile:@"splash"];
    //DebugLog(@"\n data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
        [xmlParser setDelegate:self];
        [xmlParser parse];
    }
}

-(void)sendHttpRequest:(NSString *)urlString
{
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:YES timeoutInterval:10.0];
	conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"didFailWithError: %@", [error localizedDescription]);
    [self connectionDidFinishLoading:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connectionDidFinishLoading");
	NSString *xmlDataFromChannelSchemes;
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
        [NM_APP_DELEGATE writeToTextFile:result name:@"splash"];
		DebugLog(@"\n result:%@\n\n", result);
        
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
		responseAsyncData = nil;
        conn = nil;
        result = nil;
	} else {
        [self parseFromFile];
    }
}


#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"micra"])
	{
        imagelinkArray = [[NSMutableArray alloc] init];
	}
    else if([elementName isEqualToString:@"photo"])
    {
        [imagelinkArray  addObject:[attributeDict objectForKey:@"Link"]];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"micra"])
    {
        DebugLog(@"count------->%d",[imagelinkArray count]);
        if([imagelinkArray count] > 0)
        {
            [slider reloadPages];
            [self performSelector:@selector(startAutoAnimationTimer) withObject:nil afterDelay:5.0];
        }
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if ([autoAnimationTimer isValid]) {
        [autoAnimationTimer invalidate], autoAnimationTimer=nil;
    }
     [[self navigationController] setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:goBtn];
    [self.view bringSubviewToFront:logoimageView];
    [self.view insertSubview:glowBtn aboveSubview:imageView1];
    [self.view insertSubview:glowBtn aboveSubview:imageView2];
    [self.view bringSubviewToFront:glowBtn];
}

- (IBAction)goBtnPressed:(id)sender {
    DebugLog(@"Enter the app");
    [NM_APP_DELEGATE animationStop];
}
- (IBAction)glowBtnClicked:(id)sender {
    DebugLog(@"Enter the app");
    [NM_APP_DELEGATE animationStop];
}

#pragma mark TTSlidingPagesDataSource methods
-(int)numberOfPagesForSlidingPagesViewController:(TTScrollSlidingPagesController *)source{
    if([imagelinkArray count] == 0)
        return 1;
    else
        return [imagelinkArray count]; //just return 7 pages as an example
}

-(TTSlidingPage *)pageForSlidingPagesViewController:(TTScrollSlidingPagesController*)source atIndex:(int)index{
    UIViewController *viewController = [[NMSplashImageViewController alloc] init];
    ((NMSplashImageViewController*)viewController).imglink = [imagelinkArray objectAtIndex:index];
    return [[TTSlidingPage alloc] initWithContentViewController:viewController];
}

-(TTSlidingPageTitle *)titleForSlidingPagesViewController:(TTScrollSlidingPagesController *)source atIndex:(int)index{
    TTSlidingPageTitle *title = [[TTSlidingPageTitle alloc] initWithHeaderText:@""];
    return title;
}


#pragma glow Animation Methods
- (void) startTimer {
	if (!animationTimer) {
		animationTimerCount = 0;
		[self setGradientLocations:0];
		animationTimer = [NSTimer
                          scheduledTimerWithTimeInterval:1.0/animationFramesPerSec
                          target:self
                          selector:@selector(animationTimerFired:)
                          userInfo:nil
                          repeats:YES];
	}
}

- (void) stopTimer {
	if (animationTimer) {
		[animationTimer invalidate];
		animationTimer = nil;
	}
}
// animationTimer methods
- (void)animationTimerFired:(NSTimer*)theTimer {
	// Let the timer run for 2 * FPS rate before resetting.
	// This gives one second of sliding the highlight off to the right, plus one
	// additional second of uniform dimness
	if (++animationTimerCount == (2 * animationFramesPerSec)) {
		animationTimerCount = 0;
	}
	
	// Update the gradient for the next frame
	[self setGradientLocations:((CGFloat)animationTimerCount/(CGFloat)animationFramesPerSec)];
}

- (void) setGradientLocations:(CGFloat) leftEdge {
	// Subtract the gradient width to start the animation with the brightest
	// part (center) of the gradient at left edge of the label text
	leftEdge -= gradientWidth;
	
	//position the bright segment of the gradient, keeping all segments within the range 0..1
	gradientLocations[0] = leftEdge < 0.0 ? 0.0 : (leftEdge > 1.0 ? 1.0 : leftEdge);
	gradientLocations[1] = MIN(leftEdge + gradientWidth, 1.0);
	gradientLocations[2] = MIN(gradientLocations[1] + gradientWidth, 1.0);
	
	// Re-render the label text
	[glowBtn.titleLabel.layer setNeedsDisplay];
}

// label's layer delegate method
- (void)drawLayer:(CALayer *)theLayer
        inContext:(CGContextRef)theContext
{
	// Set the font
	const char *labelFontName = [glowBtn.titleLabel.font.fontName UTF8String];
	
	// Note: due to use of kCGEncodingMacRoman, this code only works with Roman alphabets!
	// In order to support non-Roman alphabets, you need to add code generate glyphs,
	// and use CGContextShowGlyphsAtPoint
	CGContextSelectFont(theContext, labelFontName, glowBtn.titleLabel.font.pointSize, kCGEncodingMacRoman);
    
	// Set Text Matrix
	CGAffineTransform xform = CGAffineTransformMake(1.0,  0.0,
													0.0, -1.0,
													0.0,  0.0);
	CGContextSetTextMatrix(theContext, xform);
	
	// Set Drawing Mode to clipping path, to clip the gradient created below
	CGContextSetTextDrawingMode (theContext, kCGTextClip);
	
	// Draw the label's text
	const char *text = [glowBtn.titleLabel.text cStringUsingEncoding:NSMacOSRomanStringEncoding];
	CGContextShowTextAtPoint(
                             theContext,
                             0,
                             (size_t)glowBtn.titleLabel.font.ascender,
                             text,
                             strlen(text));
    
	// Calculate text width
	CGPoint textEnd = CGContextGetTextPosition(theContext);
	
	// Get the foreground text color from the UILabel.
	// Note: UIColor color space may be either monochrome or RGB.
	// If monochrome, there are 2 components, including alpha.
	// If RGB, there are 4 components, including alpha.
	CGColorRef textColor = glowBtn.titleLabel.textColor.CGColor;
	const CGFloat *components = CGColorGetComponents(textColor);
	size_t numberOfComponents = CGColorGetNumberOfComponents(textColor);
	BOOL isRGB = (numberOfComponents == 4);
	CGFloat red = components[0];
	CGFloat green = isRGB ? components[1] : components[0];
	CGFloat blue = isRGB ? components[2] : components[0];
	CGFloat alpha = isRGB ? components[3] : components[1];
    
	// The gradient has 4 sections, whose relative positions are defined by
	// the "gradientLocations" array:
	// 1) from 0.0 to gradientLocations[0] (dim)
	// 2) from gradientLocations[0] to gradientLocations[1] (increasing brightness)
	// 3) from gradientLocations[1] to gradientLocations[2] (decreasing brightness)
	// 4) from gradientLocations[3] to 1.0 (dim)
	size_t num_locations = 3;
	
	// The gradientComponents array is a 4 x 3 matrix. Each row of the matrix
	// defines the R, G, B, and alpha values to be used by the corresponding
	// element of the gradientLocations array
	CGFloat gradientComponents[12];
	for (int row = 0; row < num_locations; row++) {
		int index = 4 * row;
		gradientComponents[index++] = red;
		gradientComponents[index++] = green;
		gradientComponents[index++] = blue;
		gradientComponents[index] = alpha * gradientDimAlpha;
	}
    
	// If animating, set the center of the gradient to be bright (maximum alpha)
	// Otherwise it stays dim (as set above) leaving the text at uniform
	// dim brightness
	if (animationTimer) {
		gradientComponents[7] = alpha;
	}
    
	// Load RGB Colorspace
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	
	// Create Gradient
	CGGradientRef gradient = CGGradientCreateWithColorComponents (colorspace, gradientComponents,
																  gradientLocations, num_locations);
	// Draw the gradient (using label text as the clipping path)
	CGContextDrawLinearGradient (theContext, gradient, glowBtn.titleLabel.bounds.origin, textEnd, 0);
	
	// Cleanup
	CGGradientRelease(gradient);
	CGColorSpaceRelease(colorspace);
}


#pragma FadeIn-Out auto Animation
- (void)imageViewTapDetected:(UIGestureRecognizer *)sender
{
//    if (![autoAnimationTimer isValid]) {
//    TAP_DETECTED = YES;
//        [slider reloadPages];
//        [slider scrollToPage:currentPageIndex animated:NO];
//        [self.view bringSubviewToFront:slider.view];
//        [self.view bringSubviewToFront:glowBtn];
//        [self.view bringSubviewToFront:logoimageView];
//        slider.view.alpha = 1.0;
//        glowBtn.hidden = FALSE;
//        logoimageView.hidden = FALSE;
//        
//        [self.view sendSubviewToBack:imageView2];
//        [self.view sendSubviewToBack:imageView1];
//        imageView1.alpha = 0.0;
//        imageView2.alpha = 0.0;
//        imageView2.hidden = TRUE;
//        imageView1.hidden = TRUE;
//    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    DebugLog(@"autoAnimationTimer %@",autoAnimationTimer);
    if (autoAnimationTimer) {
        [autoAnimationTimer invalidate], autoAnimationTimer=nil;
        TAP_DETECTED = YES;
        [slider reloadPages];
        [slider scrollToPage:currentPageIndex animated:NO];
        [self.view bringSubviewToFront:slider.view];
        [self.view bringSubviewToFront:glowBtn];
        [self.view bringSubviewToFront:logoimageView];
        slider.view.alpha = 1.0;
        glowBtn.hidden = FALSE;
        logoimageView.hidden = FALSE;
        
        [self.view sendSubviewToBack:imageView2];
        [self.view sendSubviewToBack:imageView1];
//        imageView1.alpha = 0.0;
//        imageView2.alpha = 0.0;
        imageView2.hidden = TRUE;
        imageView1.hidden = TRUE;
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    DebugLog(@"touchesEnded");
    [self performSelector:@selector(startAutoAnimationTimer) withObject:nil afterDelay:5.0];
}
-(void)startAutoAnimationTimer
{
    if (imagelinkArray.count <= 0) {
        return;
    }
    if (!autoAnimationTimer)  {
        autoAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self
                                                    selector: @selector(performAutoAnimation) userInfo:nil repeats:NO];
    }
    DebugLog(@"startAutoAnimationTimer [autoAnimationTimer isValid] %d",[autoAnimationTimer isValid]);

}
-(void)performAutoAnimation
{
    if (autoAnimationTimer) {
        [self.view sendSubviewToBack:slider.view];
        [self.view sendSubviewToBack:logoimageView];
        slider.view.alpha = 0.0;
        logoimageView.hidden = TRUE;
        
        [self.view bringSubviewToFront:imageView2];
        [self.view bringSubviewToFront:imageView1];
        [self.view bringSubviewToFront:glowBtn];
        glowBtn.hidden = FALSE;

//        imageView1.alpha = 0.0;
//        imageView2.alpha = 0.0;
        currentPageIndex = [slider getCurrentDisplayedPage];
        if (imageView1.alpha == 0.0) {
            [imageView2 setImageWithURL:[imagelinkArray objectAtIndex:currentPageIndex]
                       placeholderImage:[UIImage imageNamed:@"splash.jpg"]
                                success:^(UIImage *image) {
                                    //DebugLog(@"success");
                                }
                                failure:^(NSError *error) {
                                    //DebugLog(@"write error %@", error);
                                }];
        }else{
            [imageView1 setImageWithURL:[imagelinkArray objectAtIndex:currentPageIndex]
                       placeholderImage:[UIImage imageNamed:@"splash.jpg"]
                                success:^(UIImage *image) {
                                    //DebugLog(@"success");
                                }
                                failure:^(NSError *error) {
                                    //DebugLog(@"write error %@", error);
                                }];
        }
        imageView2.hidden = FALSE;
        imageView1.hidden = FALSE;
        if (currentPageIndex == imagelinkArray.count-1) {
            currentPageIndex = 0;
        }else{
            currentPageIndex ++;
        }
        TAP_DETECTED = NO;
        [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:currentPageIndex] afterDelay:0.0];
    }
}
-(void)startAutoAnimation:(NSNumber*)pageIndex
{
    
    DebugLog(@"index %d",[pageIndex intValue]);
    currentPageIndex = [pageIndex intValue];
    if (TAP_DETECTED || stopAnimation) {
        return;
    }
    UIImageView *preImageView;
    UIImageView *nextImageView;
    if (imageView1.alpha == 0.0) {
        preImageView = imageView2;
        nextImageView = imageView1;
    }else{
        preImageView = imageView1;
        nextImageView = imageView2;
    }
    [UIView  transitionWithView:preImageView duration:2.0  options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         preImageView.alpha = 0.0;
                         [UIView  transitionWithView:nextImageView duration:2.0  options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
                                          animations:^(void) {
                                              nextImageView.alpha = 1.0;
                                              [nextImageView setImageWithURL:[imagelinkArray objectAtIndex:[pageIndex intValue]]
                                                            placeholderImage:[UIImage imageNamed:@"splash.jpg"]
                                                                     success:^(UIImage *image) {
                                                                         //DebugLog(@"success");
                                                                     }
                                                                     failure:^(NSError *error) {
                                                                         //DebugLog(@"write error %@", error);
                                                                     }];
                                          }
                                          completion:^(BOOL finished)
                                          {
                                              if ([pageIndex intValue]== imagelinkArray.count-1) {
                                                  [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:0] afterDelay:1.0];
                                              }else{
                                                  [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:[pageIndex intValue]+1] afterDelay:1.0];
                                              }
                                              
                                          }];
                     }
                     completion:^(BOOL finished)
                     {}];
    preImageView = nil;
    nextImageView = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self stopTimer];
    [self setGoBtn:nil];
    [self setLogoimageView:nil];
    [self setGlowBtn:nil];
    [self setImageView1:nil];
    [self setImageView2:nil];
    [super viewDidUnload];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)dealloc {
	[self stopTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
