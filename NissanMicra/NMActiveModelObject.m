//
//  NMActiveModelObject.m
//  NissanMicra
//
//  Created by Kirti Nikam on 19/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMActiveModelObject.h"

@implementation NMActiveModelObject
@synthesize Feature;
@synthesize XE;
@synthesize XL;
@synthesize XV;
@synthesize XV_S;
@synthesize is_same;
@end
