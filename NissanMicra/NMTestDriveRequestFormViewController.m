//
//  NMTestDriveRequestFormViewController.m
//  NissanMicra
//
//  Created by Kirti Nikam on 04/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMTestDriveRequestFormViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "constants.h"
#import "NMAppDelegate.h"
#import "MBProgressHUD.h"

#define TESTDRIVE_POST_LINK @"http://nissanmicra.co.in/micra-data/testdrive.php"

@interface NMTestDriveRequestFormViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation NMTestDriveRequestFormViewController
@synthesize headerLabel,infoLabel;
@synthesize nameTextField,mobileTextField,emailTextField,cityTextField,modelTextField;
@synthesize submitBtn;
@synthesize scrollView;
@synthesize progressHUD;
@synthesize modelArray;
@synthesize modelPickerView;
@synthesize selectedCategory;
@synthesize trasparentView;
@synthesize responseString;
@synthesize downarrowImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        DebugLog(@"already in Portrait");
    }
    else
    {
        UIViewController *c = [[UIViewController alloc]init];
        [self presentModalViewController:c animated:NO];
        [self dismissModalViewControllerAnimated:NO];
    }
    scrollView.frame = CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height);
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
     if (connection != nil) {
        [connection cancel];
        connection = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self changeButtonFontAndTextColor:submitBtn];
    [self changeLabelFontAndTextColor:headerLabel];
    [self changeLabelFontAndTextColor:infoLabel];
    
    headerLabel.font = HELVETICA_FONT(16);
    infoLabel.font = HELVETICA_FONT(10);
    
//    headerLabel.shadowColor = [UIColor blackColor];
//    headerLabel.shadowOffset = CGSizeMake(0, 0.5);
//    infoLabel.shadowColor = [UIColor blackColor];
//    infoLabel.shadowOffset = CGSizeMake(0, 0.1);

    [self changeTextFieldFontAndTextColor:nameTextField];
    [self changeTextFieldFontAndTextColor:mobileTextField];
    [self changeTextFieldFontAndTextColor:emailTextField];
    [self changeTextFieldFontAndTextColor:cityTextField];
    [self changeTextFieldFontAndTextColor:modelTextField];

    nameTextField.keyboardType      = UIKeyboardTypeDefault;
    mobileTextField.keyboardType    = UIKeyboardTypePhonePad;
    emailTextField.keyboardType     = UIKeyboardTypeEmailAddress;
    cityTextField.keyboardType      = UIKeyboardTypeDefault;
   // modelTextField.enabled = FALSE;
    
    modelArray = [[NSMutableArray alloc] init];
    [modelArray addObject:PETROL];
    [modelArray addObject:DIESEL];
    [modelArray addObject:ACTIVE];
    [self addPickerWithDoneButton];
    selectedCategory = [modelArray objectAtIndex:0];

    modelTextField.text = selectedCategory;
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.scrollView  addGestureRecognizer:viewTap];
    
    scrollView.contentSize = CGSizeMake(self.view.bounds.size.width,self.view.bounds.size.height);
    scrollView.scrollsToTop = YES;
    
    trasparentView.backgroundColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:180/255.0 alpha:0.5];
}
- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    [self scrollTobottom];
}

-(void)changeLabelFontAndTextColor:(UILabel *)lbl
{
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor whiteColor];
}

-(void)changeTextFieldFontAndTextColor:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
    textField.backgroundColor = [UIColor whiteColor];
    textField.font = HELVETICA_FONT(14);
    textField.layer.borderColor = [UIColor whiteColor].CGColor;
    textField.layer.borderWidth = 1.0;
    textField.delegate = self;
    //textField.borderStyle = UITextBorderStyleNone;
    textField.background =  [UIImage imageNamed:@"field.jpg"];
    if (textField != modelTextField) {
        textField.clearButtonMode = UITextFieldViewModeUnlessEditing;
    }
}

-(void)changeButtonFontAndTextColor:(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:TABLE_SELECTION_COLOR forState:UIControlStateHighlighted];
    [btn setBackgroundColor:[UIColor blackColor]];
    btn.titleLabel.font = HELVETICA_FONT(20);
//    btn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
//    btn.layer.borderWidth = 2.0;
}

-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [scrollView setContentOffset:bottomOffset animated:YES];
}
-(void)scrollTotop : (int) value
{
    CGPoint topOffset = CGPointMake(0, value);
    [scrollView setContentOffset:topOffset animated:YES];
}
#pragma mark Picker Button method
#pragma mark Bar Button method
- (void) show_model_Clicked{
    UISegmentedControl *doneButton = (UISegmentedControl *)[actionSheet viewWithTag:1000];
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    modelPickerView.frame = CGRectMake(modelPickerView.frame.origin.x,modelPickerView.frame.origin.y,self.view.frame.size.width,216);
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, self.view.frame.size.width,485)];
}

- (void)doneFilterBtnClicked:(id)sender
{
    downarrowImageView.hidden = NO;
    selectedCategory = [modelArray objectAtIndex:[modelPickerView selectedRowInComponent:0]];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    modelTextField.text = selectedCategory;
    [self tapDetected:nil];
}

- (void)cancelFilterBtnClicked:(id)sender
{
    downarrowImageView.hidden = NO;
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    [self tapDetected:nil];
}

#pragma Model Picker method
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    modelPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    modelPickerView.showsSelectionIndicator = YES;
    modelPickerView.dataSource = self;
    modelPickerView.delegate = self;
    [actionSheet addSubview:modelPickerView];
    
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(self.view.frame.size.width-60, 7.0f, 50.0f, 30.0f);
    doneButton.tag = 1000;
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    [modelPickerView reloadAllComponents];
    if([modelPickerView numberOfRowsInComponent:0] != -1 && modelArray.count > 0)
    {
        [modelPickerView selectRow:0 inComponent:0 animated:YES];
        selectedCategory = [modelArray objectAtIndex:0];
    }
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [modelArray count];
}

//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
//{
//    return self.view.frame.size.width;
//}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    //DebugLog(@"titleForRow: categorypickerarray %@",categoryObjectsArray);
    return [modelArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{   
}

#pragma TextField Delegate Methods
- (IBAction)EditingChanged:(UITextField *)textField {
//    if ([textField.text length] == 0) {
//        textField.text = [NSString stringWithFormat:@"  %@",textField.text];
//    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//    if ([textField.text length] == 0) {
//        textField.text = [NSString stringWithFormat:@"  %@",textField.text];
//    }
    if ([textField isEqual:modelTextField]) {
        [modelTextField resignFirstResponder];
        downarrowImageView.hidden = YES;
        [self show_model_Clicked];
    }
    
    if(textField.frame.origin.y > scrollView.contentOffset.y)
        [scrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-45) animated:YES];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:mobileTextField] && [mobileTextField.text length] > 15) {
            return NO;
    }
    else
    {
        return YES;
    }
}
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    if ([textField.text isEqualToString:@"  "]) {
//        textField.text = @"";
//    }
//    return YES;
//}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == nameTextField) {
        [mobileTextField becomeFirstResponder];
	} else if (textField == mobileTextField) {
        [emailTextField becomeFirstResponder];
	} else if (textField == emailTextField) {
        [cityTextField becomeFirstResponder];
	} else if (textField == cityTextField) {
        [modelTextField becomeFirstResponder];
	} else if (textField == modelTextField) {
        [modelTextField resignFirstResponder];
        //[self show_model_Clicked];
	}
   	return YES;
}
-(BOOL)validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}
-(BOOL)validatePhoneNumber:(NSString *) phoneString {
    //  NSString *phoneRegex=@"[+]+91[0-9]{10}";
    NSString *phoneRegex=@"[0-9]{16}";
    NSPredicate *noTest=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [noTest evaluateWithObject:phoneString];
}
-(BOOL)validateInputs
{
//    if([nameTextField.text isEqualToString:@""] && [mobileTextField.text isEqualToString:@""] && [emailTextField.text isEqualToString:@""] && [cityTextField.text isEqualToString:@""])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
//                                                            message:@"Please fill all the fields."
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//        [alertView show];
//        return FALSE;
//    }else{
        if([nameTextField.text isEqualToString:@""])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Please enter your name."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            return FALSE;
        }
        if([mobileTextField.text isEqualToString:@""])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Please enter mobile number."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            return FALSE;
        }else{
            if([mobileTextField.text length] < 10)
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:@"Number should be 10 digits."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
                return FALSE;
            }
        }
        if([emailTextField.text isEqualToString:@""]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Please enter email."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            return FALSE;
        }else{
            if(![self validateEmail:emailTextField.text])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:@"Please enter a valid email."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
                return FALSE;
            }
        }
        if([cityTextField.text isEqualToString:@""]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Please enter city."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            return FALSE;
        }
    return TRUE;
}
- (IBAction)submitBtnClicked:(id)sender {
    
    if ([self validateInputs]) {
        if ([NM_APP_DELEGATE networkavailable])
        {
            [self sendHttpRequest:TESTDRIVE_POST_LINK];
        }
        else
        {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    }
}


- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
-(void)sendHttpRequest:(NSString *)urlString
{
    [self showProgressHUDWithMessage:@"Proccesing"];
    DebugLog(@"URL : %@",urlString);
    
     NSMutableData *postData = [[NSMutableData alloc] init];
//    [postData appendData:[[NSString stringWithFormat:@"{\"name\":\"%@\",\"mobileno\":\"%@\",\"email\":\"%@\",\"city\":\"%@\",\"source\":\"ios\",\"model\":\"micra\",\"submit\":\"submit\"}",nameTextField.text,mobileTextField.text,emailTextField.text,cityTextField.text] dataUsingEncoding:NSUTF8StringEncoding]];
//    DebugLog(@"postData : %@",[NSString stringWithFormat:@"{\"name\":\"%@\",\"mobileno\":\"%@\",\"email\":\"%@\",\"city\":\"%@\",\"source\":\"ios\",\"model\":\"micra\",\"submit\":\"submit\"}",nameTextField.text,mobileTextField.text,emailTextField.text,cityTextField.text]);
    
    DebugLog(@"postData : %@",[NSString stringWithFormat:@"name=%@&mobileno=%@&email=%@&city=%@&source=ios&model=%@&submit=submit",nameTextField.text,mobileTextField.text,emailTextField.text,cityTextField.text,[selectedCategory lowercaseString]]);
    
    [postData appendData:[[NSString stringWithFormat:@"name=%@&mobileno=%@&email=%@&city=%@&source=ios&model=%@&submit=submit",nameTextField.text,mobileTextField.text,emailTextField.text,cityTextField.text,[selectedCategory lowercaseString]] dataUsingEncoding:NSUTF8StringEncoding]];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:TESTDRIVE_POST_LINK]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%d", postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"didFailWithError: %@", [error localizedDescription]);
    [self connectionDidFinishLoading:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)localconnection
{
    DebugLog(@"connectionDidFinishLoading");
    [self hideProgressHUD:YES];
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
        
        NSError *error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
        DebugLog(@"\n json:%d %@ \n\n",json.count, json);
        if (json != nil && json.count > 0) {
            for (NSString* attributeDict in json.allKeys)
            {
                DebugLog(@"\n attributeDict:%@\n\n", attributeDict);
                responseString = [[json valueForKey:@"err_status"] lowercaseString];
                if([attributeDict isEqualToString:@"message"])
                {
                    NSString *message = [json valueForKey:@"message"];
                    DebugLog(@"\n message:%@\n\n", message);
                    DebugLog(@"found");
                    UIAlertView *alertView = [[UIAlertView alloc]
                                              initWithTitle:ALERT_TITLE
                                              message:message
                                              delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                    [alertView show];
                    [Flurry logEvent:@"TestDrive_Request_Event"];
                }
            }
            //        NSRange temprange = [result rangeOfString:@"message"];
            //        DebugLog(@"location %d length %d",temprange.location,temprange.length);
            //        if ((temprange.location != NSNotFound))
            //        {
            //            DebugLog(@"found");
            //            UIAlertView *errorView = [[UIAlertView alloc]
            //                                      initWithTitle:ALERT_TITLE
            //                                      message:@"Thanks, your details were submitted successfully, We will get in touch with you soon."
            //                                      delegate:self
            //                                      cancelButtonTitle:@"OK"
            //                                      otherButtonTitles:nil];
            //            [errorView show];
            //            [Flurry logEvent:@"TestDrive_Request_Event"];
            //        }
            
            result = nil;
            self->responseAsyncData = nil;
            connection = nil;
        }
    }
}
#pragma alertView delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([responseString hasPrefix:@"success"])
    {
        DebugLog(@"Got success");
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.width,self.view.bounds.size.height);
    }
    else
    {
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.width,self.view.bounds.size.height+180);
    }
}

#pragma Device Orientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
   return UIInterfaceOrientationMaskPortrait;
    //return UIInterfaceOrientationMaskAll;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setHeaderLabel:nil];
    [self setInfoLabel:nil];
    [self setNameTextField:nil];
    [self setMobileTextField:nil];
    [self setEmailTextField:nil];
    [self setCityTextField:nil];
    [self setSubmitBtn:nil];
    [self setScrollView:nil];
    [self setModelTextField:nil];
    [self setModelPickerView:nil];
    [self setTrasparentView:nil];
    [self setDownarrowImageView:nil];
    [super viewDidUnload];
}

@end
