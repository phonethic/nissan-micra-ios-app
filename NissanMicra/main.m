//
//  main.m
//  NissanMicra
//
//  Created by Kirti Nikam on 18/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NMAppDelegate class]));
    }
}
