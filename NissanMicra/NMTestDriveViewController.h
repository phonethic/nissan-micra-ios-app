//
//  NMTestDriveViewController.h
//  NissanMicra
//
//  Created by Kirti Nikam on 04/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMTestDriveViewController : UIViewController
{
    BOOL oneTimeAnimation;
}
@property (strong, nonatomic) IBOutlet UITableView *testDriveTableView;
@property (strong, nonatomic) NSArray *listArray;
@end
