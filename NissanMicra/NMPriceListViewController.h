//
//  NMPriceListViewController.h
//  NissanMicra
//
//  Created by Rishi on 21/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@class NMPriceListObject;
@interface NMPriceListViewController : UIViewController <NSXMLParserDelegate,UIPickerViewDelegate,UIPickerViewDataSource> {
    NSMutableData *responseAsyncData;
    NSXMLParser *xmlParser;
    NMPriceListObject *priceObj;
    sqlite3 *micraDB;
    UIActionSheet *actionSheet;
    NSURLConnection *priceconnection;
}
@property (strong, nonatomic) NSMutableArray *pricelistArray;
@property (strong, nonatomic) NSMutableArray *stateArray;
@property (strong, nonatomic) NSMutableArray *cityArray;
@property (strong, nonatomic) NSMutableArray *modelArray;
@property (retain, nonatomic) UIPickerView *statePicker;
@property (retain, nonatomic) UIPickerView *cityPicker;
@property (strong, nonatomic) UIPickerView *modelPicker;
@property (copy,nonatomic) NSString *selectedState;
@property (copy,nonatomic) NSString *selectedCity;
@property (copy,nonatomic) NSString *selectedCategory;
@property (strong, nonatomic) IBOutlet UIButton *stateBtn;
@property (strong, nonatomic) IBOutlet UIButton *cityBtn;
@property (strong, nonatomic) IBOutlet UITableView *pricelistTableView;

- (IBAction)stateBtnPressed:(id)sender;
- (IBAction)cityBtnPressed:(id)sender;

@end
